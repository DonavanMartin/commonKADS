package model.element.commonKADS;

import app.Helper.ElementStates;
import java.util.ArrayList;
import org.w3c.dom.Document;
import view.element.Element;
import view.shape.simpleRectangle;
import view.ui.Logs;

public class KnowledgeAsset extends Node {
    private boolean isRightForm;
    private boolean isRightPlace;
    private boolean isRightTime;
    private boolean isRightQuality;
    
    private ArrayList<Task> usedIn;
    private Agent possededBy;
    private Element owner;

    public KnowledgeAsset(Logs log){
        super(log);
        type = ElementStates.KNOWLEDGEASSET;    // IMPORTANT !
        setTypeName(type);
        usedIn = new ArrayList<>();
        shape = new simpleRectangle();
    }

    public boolean isIsRightForm() {
        return isRightForm;
    }

    public void setIsRightForm(boolean isRightForm) {
        this.isRightForm = isRightForm;
    }

    public boolean isIsRightPlace() {
        return isRightPlace;
    }

    public void setIsRightPlace(boolean isRightPlace) {
        this.isRightPlace = isRightPlace;
    }

    public boolean isIsRightTime() {
        return isRightTime;
    }

    public void setIsRightTime(boolean isRightTime) {
        this.isRightTime = isRightTime;
    }

    public boolean isIsRightQuality() {
        return isRightQuality;
    }

    public void setIsRightQuality(boolean isRightQuality) {
        this.isRightQuality = isRightQuality;
    }

    public ArrayList<Task> getUsedIn() {
        return usedIn;
    }

    public void setUsedIn(ArrayList<Task> usedIn) {
        this.usedIn = usedIn;
    }

    public Agent getPossededBy() {
        return possededBy;
    }

    public void setPossededBy(Agent possededBy) {
        this.possededBy = possededBy;
    }
    
    public void addUsedIn(Task task) {
        this.usedIn.add(task);
    }
    public void removeUsedIn(Task task) {
        this.usedIn.remove(task);
    }
    
    //--------------\\
    //   OVERRIDES  \\
    //--------------\\
    @Override
    public int isOwnable(){
        return isOwnable;
    }
    
    @Override
    public Element writeXML(Document xmlDoc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Element getOwnableElement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
