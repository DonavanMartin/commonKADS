package model.element.commonKADS;

import app.Helper.ElementStates;
import view.shape.FilledCircle;
import view.ui.Logs;

public class Initial extends Node {
    public Initial(Logs log) {
        super(log);
        type = ElementStates.INITIAL_NODE;
        setTypeName(type);
        shape = new FilledCircle();
    }
    @Override
    public int isOwnable(){
        return isOwnable;
    }  
    //--------------------\\
    // CoomonKADS section \\
    //--------------------\\

    //------------\\
    //  OVERRIDES \\
    //------------\\
}
 