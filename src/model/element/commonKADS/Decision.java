package model.element.commonKADS;

import app.Helper.ElementStates;
import view.element.Element;
import view.shape.FilledLosange;
import view.ui.Logs;
import view.workSheets.Decision_worksheet;

public class Decision extends Node {
    private Decision_worksheet decision;
    private boolean inOutWarning;
    public Decision(Logs log) {
        super(log);
        type = ElementStates.DECISION;
        setTypeName(type);
        decision = new Decision_worksheet(this, log);
        worksheets.add(decision);
        inOutWarning = true;
        shape = new FilledLosange();
    }

    public Decision_worksheet getDecision() {
        return decision;
    }

    public boolean isInOutWarning() {
        return inOutWarning;
    }
    
    @Override
    public int isOwnable(){
        return isOwnable;
    }  
    //--------------------\\
    // CoomonKADS section \\
    //--------------------\\
    
    
    //------------\\
    //  OVERRIDES \\
    //------------\\
    // INPUTS
    @Override
    public void addInput(Element e){
        if(e instanceof Task){
            decision.setInputsTasks((Task)e);
        }
        verifyWarningInputOutput();
    } 
    @Override
    public void removeInput(Element e){
        if(e instanceof Task){
            decision.setInputsTasks(null);
        }
        verifyWarningInputOutput();
    }
    
    // OUTPUTS
    @Override
    public void addOutput(Element e){
        if(e instanceof Task){
            decision.addOutputsTasks((Task)e);
        }
        verifyWarningInputOutput();
    } 
    @Override
    public void removeOutput(Element e){
        if(e instanceof Task){
            decision.removeOutputsTasks((Task)e);
        }
        verifyWarningInputOutput();
    }
    
    private void verifyWarningInputOutput(){
        if ( decision.getOutputsTasks().size()<2 
         || decision.getInputTasks() == null){
             inOutWarning = true;
        }else{
            inOutWarning = false;
        }
    }
}
