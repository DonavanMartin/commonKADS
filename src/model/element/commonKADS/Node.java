package model.element.commonKADS;

import view.element.Associable;
import org.w3c.dom.Document;
import view.element.Element;
import view.ui.Logs;
import view.element.Ownable;

public abstract class Node extends Associable implements Ownable{

    public Node(Logs log) {
        super(log);
    }
   
    @Override
    public Element writeXML(Document xmlDoc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Element getOwnableElement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }     
}
