package model.element.commonKADS;

import app.Helper.ElementStates;
import view.workSheets.OM3;
import view.workSheets.TM1;
import view.workSheets.WorkSheet;
import view.element.Associable;
import java.util.ArrayList;
import org.w3c.dom.Document;
import view.element.Element;
import view.shape.simpleRoundRectangle;
import view.ui.Logs;
import view.element.Ownable;
public class Task extends Node {
     // TODO
    // private TaskModel model;
    private TM1 tm1;
    private OM3 om3;
    private Element owner;

    public Task(Logs log) {
        super(log);
        type = ElementStates.TASK; // IMPORTANT !
        setTypeName(type);
        tm1 = new TM1(this, log);
        om3 = new OM3(this, log);
        worksheets.add(tm1);
        worksheets.add(om3);
        shape = new simpleRoundRectangle();
    }
    
    // INPUTS
    @Override
    public void addInput(Element e){
        if(e instanceof KnowledgeAsset){
            tm1.addInputsKnowledgeAsset((KnowledgeAsset)e);
            om3.addInputsKnowledgeAsset((KnowledgeAsset)e);
        }else if(e instanceof Task){
            tm1.addInputsTasks((Task)e);
        }
    } 
    @Override
    public void removeInput(Element e){
        if(e instanceof KnowledgeAsset){
             tm1.removeInputsKnowledgeAsset((KnowledgeAsset)e);
             om3.removeInputsKnowledgeAsset((KnowledgeAsset)e);
        }else if(e instanceof Task){
            tm1.removeInputsTasks((Task)e);
        }
    }
    
    // OUTPUTS
    public void addOutput(Element e){
        if(e instanceof KnowledgeAsset){
             tm1.addOutputsKnowledgeAsset((KnowledgeAsset)e);
             om3.addOutputsKnowledgeAsset((KnowledgeAsset)e);
        }else if(e instanceof Task){
            tm1.addOutputsTasks((Task)e);
      
        }
    } 
    public void removeOutput(Element e){
        if(e instanceof KnowledgeAsset){
             tm1.removeOutputsKnowledgeAsset((KnowledgeAsset)e);
             om3.removeOutputsKnowledgeAsset((KnowledgeAsset)e);
        }else if(e instanceof Task){
            tm1.removeOutputsTasks((Task)e);
        }
    }
    
    
    
    
    @Override
    public int isOwnable(){
        return isOwnable;
    }  
    @Override
    public void setName(String name){
        super.setName(name);
        tm1.setName(name);
    }
    
    @Override
    public void setName(String newName, int conceptNumber){
        super.setName(newName, conceptNumber);
        setName(name);
    }

    public ArrayList<WorkSheet> getWorkSheet(){
        return worksheets;
    } 

    @Override
    public Element writeXML(Document xmlDoc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Element getOwnableElement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
