package model.element.commonKADS;

import app.Helper.ElementStates;
import view.shape.MultiCircle;
import view.ui.Logs;

public class Final extends Node{
    public Final(Logs log) {
        super(log);
        type = ElementStates.FINAL_NODE;
        setTypeName(type);
        shape = new MultiCircle();
    }
    @Override
    public int isOwnable(){
        return isOwnable;
    }  
    //--------------------\\
    // CoomonKADS section \\
    //--------------------\\

    
    //------------\\
    //  OVERRIDES \\
    //------------\\

}
 