package model.element.commonKADS;

import view.element.Association;
import view.ui.Logs;
import view.workSheets.BasicProperties;

public class DecisionFlow extends Association{
    public DecisionFlow(Logs log){
        super(false, log);
        properties = new BasicProperties(this,log);
        worksheets.add(properties);
    }   
}
