package model.element.commonKADS;

import app.Helper.ElementStates;
import java.awt.Point;
import java.util.ArrayList;
import org.w3c.dom.Document;
import view.element.Corner;
import view.element.Element;
import view.shape.ElementShape;
import view.shape.TitleledRectangle;
import view.ui.Logs;
import view.element.Ownable;

public class Agent extends Element implements Ownable {

    private ArrayList<KnowledgeAsset> possededKnowledge;
    private ArrayList<Task> performIn;
    
    public Agent(Logs log) {
        super(log);
        type = ElementStates.AGENT;
        setTypeName(type);
        
        possededKnowledge = new ArrayList<>();
        performIn         = new ArrayList<>();
        
        shape = new TitleledRectangle();
    }
    
    
    public ElementShape getShape(){
        return shape;
    }
    //-------------------\\
    //     UI section    \\
    //-------------------\\
    
    public void setImediateRight(ElementShape s){
        //
        //   a ---------- b ----------e
        //   |      s     |    this   |
        //   c------------d-----------f
        //
        
        //Point a = s.getUpperLeftHandle().getLocation();
        Point b = s.getUpperRightHandle().getLocation();
        //Point c = s.getLowerLeftHandle().getLocation();
        Point d = s.getLowerRightHandle().getLocation();
        
        
        //this.shape.resizeUpperLeft((int)b.getX(), (int)b.getY());
        //this.shape.resizeUpperRight((int)b.getX(),(int)b.getY());
        
        //this.shape.resizeLowerLeft((int)d.getX(), (int)d.getY());
        //this.shape.resizeLowerRight((int)d.getX(), (int)d.getY());
        this.shape.getUpperLeftHandle() .setLocation(new Point((int)b.getX()             , (int)b.getY()));
        this.shape.getUpperRightHandle().setLocation(new Point((int)b.getX()+s.getWidth(), (int)b.getY()));
        this.shape.getLowerLeftHandle() .setLocation(new Point((int)d.getX(),              (int)d.getY()));
        this.shape.getLowerRightHandle().setLocation(new Point((int)d.getX()+s.getWidth(), (int)d.getY()));
        //this.shape.cumputAllDragLimit();
        shape.setLocation(b);
        setLocation(b);
        updateShape();
        
        logs.printInfo("Agent set immediate right from this location: "+ s.getLocation());
    }
    
    public void updateShape(){
        setLocation(this.shape.getUpperLeftHandle().getLocation());
        int x = (int)this.shape.getLocation().getX();
        int y = (int)this.shape.getLocation().getY();
        setBounds(x, y);
    }
    
    public void updateMovingCorner(Point p){
        this.shape.getUpperLeftHandle() .setLocation(new Point((int)p.getX()             , (int)p.getY()));
        this.shape.getUpperRightHandle().setLocation(new Point((int)p.getX()+getWidth()  , (int)p.getY()));
        this.shape.getLowerLeftHandle() .setLocation(new Point((int)p.getX()             , (int)p.getY()+shape.NAME_HEIGHT+shape.contentHeight));
        this.shape.getLowerRightHandle().setLocation(new Point((int)p.getX()+getWidth()  , (int)p.getY()+shape.NAME_HEIGHT+shape.contentHeight));
    }
    
    public void setUpperLimit(Point p){
        Corner uL = shape.getUpperLeftHandle();
        
        Point uLeft  = new Point((int)uL.getLocation().getX() , (int)p.getY());
        this.shape.resizeUpperY((int) p.getY());
        shape.setLocation(uLeft);
        setLocation(uLeft);
        updateShape();
    }
    
    public void setLowerLimit(Point p){
        this.shape.resizeLowerY((int) p.getY());
        updateShape();
    }
    
    public void resizeLeft(int newX){
        this.shape.resizeLeft(newX);
        updateShape();
    }
    public void resizeRigth(int newX){
        this.shape.resizeRight(newX);
        updateShape();
    }
    
    public int getMidX(){
        Corner uL = shape.getUpperLeftHandle();
        Corner uR = shape.getUpperRightHandle();
        double l = (int)uL.getLocation().getX();
        double r = (int)uR.getLocation().getX();
        return (int)((l+r)/2);
    }
    
    public Point getRightPointX(){
        return shape.getUpperRightHandle().getLocation();
    }
    public Point getLeftPointX(){
        return shape.getUpperLeftHandle().getLocation();
    }
    
    //--------------------\\
    // CoomonKADS section \\
    //--------------------\\
    
    public ArrayList<KnowledgeAsset> getPossededKnowledge() {
        return possededKnowledge;
    }
    
    public void setPossededKnowledge(ArrayList<KnowledgeAsset> possededKnowledge) {
        this.possededKnowledge = possededKnowledge;
    }
    
    public ArrayList<Task> getPerformIn() {
        return performIn;
    }
    
    public void setPerformIn(ArrayList<Task> performIn) {
        this.performIn = performIn;
    }
    
    public void addKnowledgeAsset(KnowledgeAsset knowledgeAsset){
        this.possededKnowledge.add(knowledgeAsset);
    }
    
    public void addPerformIn(Task task){
        this.performIn.add(task);
    }
    
    public void removeKnowledgeAsset(KnowledgeAsset knowledgeAsset){
        this.possededKnowledge.remove(knowledgeAsset);
    }
    
    public void removePerformIn(Task task){
        this.performIn.remove(task);
    }
    
    
    //------------\\
    //  OVERRIDES \\
    //------------\\
    
    @Override
    public int isOwnable(){
        return isOwnable;
    }
    
    @Override
    public Element writeXML(Document xmlDoc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Element getOwnableElement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void addOwnableElement(Ownable ownableElement) {
        if( -1 == super.listOwnableElement.indexOf(ownableElement)){
            super.addOwnableElement(ownableElement);
            if(ownableElement instanceof Task){
                performIn.add((Task)ownableElement);
            }
            else if(ownableElement instanceof KnowledgeAsset){
                possededKnowledge.add((KnowledgeAsset)ownableElement);
            }
        }
    }
}