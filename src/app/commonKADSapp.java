package app;

import view.ui.Logs;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import controller.FileControl;
import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import view.ui.mainPanel;

public class commonKADSapp extends JFrame {
    private FileControl fileController;
    private mainPanel mainPanel;
    private Logs log; 
    private final String TITLE = "commonKADS";
    private static commonKADSapp app = null;
    
    public static void fixMyDocumentsRights() {
        File defDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getDefaultDirectory();
        String defDirString = "\"" + defDir.toString() + "\"";
        String toexec = "cmd /c attrib -r /s " + defDirString;
        try {
            Runtime.getRuntime().exec(toexec);
        } catch (IOException e){
            System.err.println(e.getMessage());
        }
    }
    public static void main(String[] args) {
        if (System.getProperty("os.name").equals("Windows XP"));
            fixMyDocumentsRights();
         commonKADSapp.application().execStandalone();
    }
   public static commonKADSapp application() {
        if (null == commonKADSapp.app)
            commonKADSapp.app = new commonKADSapp();
        return commonKADSapp.app;
    }
    
    public void execStandalone() {
        // Start engine:
        setTitle(TITLE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        execEngine();
        add(mainPanel, BorderLayout.CENTER);

        setVisible(true);
        pack();
        setResizable(true);
        addCloseListener();
        log.printInfo("initialization: JFrame");

    }
    
    public synchronized void execEngine() {  
        log = new Logs();
       // fileController = new FileControl();
       // fileController.setUserInterface(this);
        mainPanel = new mainPanel(fileController, log);
    }
    
    public synchronized void update() {
        mainPanel.getCanvas().update();
    }
    
    public Point getFrameLocation() {
        return getLocation();
    }
    
    public void setFileName(String name) {
        setTitle(TITLE + name);
    }
    
    private void addCloseListener(){
        class CloseListener extends WindowAdapter {
            @Override
            public void windowClosing(WindowEvent e) {
               // fileController.closeFile();
            }
        }
        WindowListener windowListener = new CloseListener();
        addWindowListener(windowListener);
    }
}
