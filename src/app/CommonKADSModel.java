package app;

import model.element.commonKADS.KnowledgeAsset;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import view.element.Element;
import app.Helper.ElementStates;
import view.element.Association;
import controller.Control;
import model.element.commonKADS.Agent;
import model.element.commonKADS.Task;
import view.element.Ownable;

public class CommonKADSModel {
    private Control control;
    private ArrayList<Association> listAssociation;
    private ArrayList<Element> listElement;
    private ArrayList<Element> listOwnable;
    
    // CommonKADS
    //private ArrayList<Activity>       listActivity;
    //private ArrayList<OpaqueAction>   listOpaqueAction;
    //private ArrayList<Input>          listInput;
    //private ArrayList<Output>         listOutput;
    //private ArrayList<InitialNode>    listInitialNode;
    //private ArrayList<FinalNode>      listFinalNode;
    //private ArrayList<ControlFlow>    listControlFlow;
    //private ArrayList<ObjectFlow>     listObjectFlow;
    private ArrayList<Task> listTask;
    private ArrayList<Agent>            listAgent;
    private ArrayList<KnowledgeAsset>   listKnowledgeAsset;
    
    private int currentProcessIdNumber;
    private int currentAgentIdNumber;
    private int currentTaskIdNumber;
    private int currentElementIdNumber;
    
    private boolean hasChanged;
    
    public CommonKADSModel(Control control) {
        this.control = control;
        listAssociation = new ArrayList<>();
        listElement = new ArrayList<>();
        listOwnable = new ArrayList<>();
        
        // CommonKADS
        //listActivity = new ArrayList<Activity>();
        //listOpaqueAction = new ArrayList<OpaqueAction>();
        //listInput = new ArrayList<Input>();
        //listOutput = new ArrayList<Output>();
        //listInitialNode = new ArrayList<InitialNode>();
        //listFinalNode = new ArrayList<FinalNode>();
        //listControlFlow = new ArrayList<ControlFlow>();
        //listObjectFlow = new ArrayList<ObjectFlow>();
        listTask = new ArrayList<>();
        listAgent   = new ArrayList<>();
        listKnowledgeAsset = new ArrayList<>();
        
        currentProcessIdNumber  = 0; // +
        currentAgentIdNumber    = 0; // +
        currentTaskIdNumber     = 0; // +
        currentElementIdNumber  = 0; // =
        
        hasChanged = false;
    }
    
    public void add(Element element) {
        listElement.add(element);
        if(element.isOwnable() == ElementStates.OWNABLEELEMENT){
            listOwnable.add(element);
        }
        
        switch (element.getType()) {
            case ElementStates.AGENT:           listAgent         .add((Agent)         element);   break;
            case ElementStates.TASK:            listTask          .add((Task)          element);   break;
            case ElementStates.KNOWLEDGEASSET:  listKnowledgeAsset.add((KnowledgeAsset)element);   break;
            default:control.getLog().printError("Not set YEt "+ ElementStates.getName( element.getType()));; 
        }
    }

    public ArrayList<Element> getListElement() {
        return listElement;
    }



////////   ////////   ////////   ////////   ////////   ////////   ////////   ////////   ////////
    // TO DELETE \\
    // This logic will conflict !
    
    public String getNextElementId() {
        return "E" + Integer.toString(++currentElementIdNumber);
    }    
    // Conflict !
    public String getNextTaskId() {
        return "T" + Integer.toString(++currentTaskIdNumber);
    }
    public String getNextAgentId() {
        return "A" + Integer.toString(++currentAgentIdNumber);
    }
    public String getNextProcessId() {
        return "P" + Integer.toString(++currentProcessIdNumber);
    }
    // String id : charAt(0) will cause problem for sure!
    public Element find(String id) {
        Element element = null;
        
        switch(id.charAt(0)) {
            default : control.getLog().printError("Find id --> Not implemented yet ! ");
        }
        return element;
    }
////////   ////////   ////////   ////////   ////////   ////////   ////////   ////////   ////////
    
    
    
    
    public Dimension getBounds() {
        Rectangle bounds = new Rectangle();
        int nb_elements = listElement.size();
        if(nb_elements > 0){
            for(int i = 0; i < nb_elements; i++){
                bounds.add(listElement.get(i).getBounds());
            }
        }
        return new Dimension((int)bounds.getWidth(), (int)bounds.getHeight());
    }
    
    private void switchElement(List list, Element e) {
        int i = list.indexOf(e);
        int lastI = list.size()-1;
        Element lastE = (Element) list.get(lastI);
        list.set(lastI, e);
        list.set(i, lastE);
    }
    
    public void remove(Element element) {
        listElement.remove(element);
        removeInList(element);
        
        if(element.isOwnable() == ElementStates.OWNABLEELEMENT){
            removeOwnable(element);
        }
    }
    
    private void removeOwnable(Element element){
        listOwnable.remove(element);
        listElement.stream().filter((e) -> (e != element)).forEach((e) -> {
            e.removeOwnableElement((Ownable) element);
        });
    }
    
    private void removeInList(Element element){
        switch (element.getType()) {
            case ElementStates.AGENT:           listAgent         .remove((Agent)         element); break;
            case ElementStates.TASK:            listTask          .remove((Task)          element); break;
            case ElementStates.KNOWLEDGEASSET:  listKnowledgeAsset.remove((KnowledgeAsset)element); break;
            default: control.getLog().printError("removeInList Not Set Yet for: "+ElementStates.getName(element.getType()));break;
        }
    }
    
    public void setOnTop(Element element) {
        switch (element.getType()) {
            case ElementStates.TASK:            switchElement(listTask,           element); break;
            case ElementStates.KNOWLEDGEASSET:  switchElement(listKnowledgeAsset, element); break;
            //default: control.getLog().printError("setOnTop Not Set Yet for: "+ElementStates.getName(element.getType()));break;
        }
    }
    
    public void draw(Graphics2D g2) {
        listElement.stream().forEach((element) -> { 
            element.draw(g2);
        });
    }
    
    //-----------------------\\
    // * SETTERS + GETTERS * \\
    //-----------------------\\
    
    public void sethasChanged(boolean changed) {
        hasChanged = changed;
    }
    
    public int getCurrentElementIdNumber() {
        return currentElementIdNumber;
    }
    
    public void setCurrentProcessId(int no){
        currentProcessIdNumber = no;
    }
    
    public boolean gethasChanged() {
        return hasChanged;
    }
    
    public int getCurrentProcessId(){
        return currentProcessIdNumber;
    }
    
    public int getListVisualElementSize() {
        return listElement.size();
    }
    
    public int getListOwnableElementSize() {
        return listOwnable.size();
    }
    
    public Element getElement(Point point) {
        for(Element element : listElement){
            if (element.isInSelectionZone(point)){
                return element;
            }
        }
        return null;
    }
    
    public Ownable getOwnableElement(int index) {
        return (Ownable)listOwnable.get(index);
    }
    
    public Element getVisualElement(int index) {
        if(index < listElement.size()){
            return listElement.get(index);
        }else{
            return null;
        }
    }
    
    public void addAssociation(Association a){
        listAssociation.add(a);
    }

    public ArrayList<Association> getListAssociation() {
        return listAssociation;
    }
    
    
    public void removeAssociation(Association e){
        listAssociation.remove(e);
    }

    public ArrayList<Agent> getListAgent() {
        return listAgent;
    }
}
