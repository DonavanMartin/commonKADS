package app.Helper;
import java.io.File;
import javax.swing.filechooser.FileFilter;

class ExtensionFilter extends FileFilter {
    
    public static final String EXTENSION = "kads";
    
    public ExtensionFilter() {}
    
    @Override
    public boolean accept(File file) {
        boolean accept = false;
        
        if(file.isDirectory()) {
            accept = true;
        }
        else {
            String fileName = file.toString().toLowerCase();
            int index = fileName.lastIndexOf(".");
            if(index != -1) {
                String fileExtension = fileName.substring(index + 1);
                if(fileExtension.equalsIgnoreCase(EXTENSION))
                    accept = true;
            }
        }
        return accept;
    }
    
    @Override
    public String getDescription() {
        return "CommonKADS (." + EXTENSION + ")";
    }
    
}
