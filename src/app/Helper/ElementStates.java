package app.Helper;

public class ElementStates {
    
    /*
    
    Actual definition can take  maximum of 999 (M2) X 999 (M1)
    
    We define M2-M1-M0 by model abstraction:
    Let's take an example with a << Person >> in context of an entreprise.:
        - M2 (ex: Person)
        - M1 (ex: Client, Director, Worker...)
        //- M0 (ex: productClients, serviceClients, manageDirector, adminDirector, devellopper,...)    
    
    Let's take an example with a << House >> in context of an home's house.:
        - M2 (ex: House)
        - M1 (ex: Wall, Windows, DriveWay)
        //- M0 (ex: brickFacade1, asphlapteDriveway, aluminiumWindow1, aluminiumWindow2, woodWindow1 ,...)     
    */
    
    public static final int NOTSET          =     -1;
                                                                                 // Basic operartion
    /*public static final int OPERATION     =      0;
    public static final int LOGICAL         =      1;
    public static final int EQUALS          =      2;
    public static final int NOTEQUALS       =      3;
    public static final int GREATERTHAN     =      4;
    public static final int LESSTHAN        =      5;
    public static final int GREATEROREQUALS =      6;
    public static final int LESSOREQUALS    =      7;    
    public static final int SUM             =      8;
    public static final int MAXIMUM         =      9;
    public static final int MINIMUM         =     10;
    public static final int OPERATOR        =     11; 
    public static final int TERM            =     12;
    public static final int ARITHMETIC      =     13;
    public static final int ADDITION        =     14;
    public static final int SUBSTRACTION    =     15;
    public static final int MULTIPLICATION  =     16;
    public static final int DIVISION        =     17;
    public static final int EXPONENT        =     18;
    public static final int CONSTANT        =     19;
    public static final int VARIABLE        =     20;*/
                                                                                // Concept
    public static final int CONCEPT         =   1000;
    public static final int MODELCONCEPT    =   1001;
    public static final int VISUALCONCEPT   =   1002; 

                                                                                // Association
    public static final int ASSOCIATION     =   2000; 
    public static final int AGGREGATION     =   2001;
    public static final int FROMASSOCIATION =   2002;
    public static final int TOASSOCIATION   =   2003;
    public static final int ASSOCIATABLE    =   2004;
                                                                                // Process
    public static final int PROCESS         =   3000;
                                                                                // Task
    public static final int TASK            =   4000;
                                                                                // Input
    public static final int INPUT           =   5000;     
    public static final int TASKINPUT       =   5001;
    public static final int KNOWLEDGEINPUT  =   5002;    
                                                                                // Output
    public static final int OUTPUT          =   6000;     
    public static final int TASKOUTPUT      =   6001;
    public static final int KNOWLEDGEOUTPUT =   6002;  
                                                                                // Node
    public static final int NODE            =   7000;
    public static final int INITIAL_NODE    =   7001;   
    public static final int FINAL_NODE      =   7002;
    
                                                                                // Flow
    public static final int FLOW            =   8000;
    public static final int KNOWLEDGE_FLOW  =   8001;
    public static final int TASK_FLOW       =   8002;
    public static final int DECISION        =   8003;                               
                                                                                // Activity
    public static final int ACTIVITY        =   9000; 
                                                                                // Agent
    public static final int AGENT           =  10000;
                                                                                // KnowledgeAsset
    public static final int KNOWLEDGEASSET  =  11000;
                                                                                // Element    
    public static final int ELEMENT         =  80000;
    public static final int OWNABLEELEMENT  =  80001;
    public static final int MODELELEMENT    =  80002;
                                                                                // UI actions 
    public static final int SELECT          =  99998; 
    public static final int DELETE          =  99999; 
    

    
    //*************\\
    // Int to Name \\
    //*************\\
    public static String getName(int state){
    switch(state){
        case NOTSET         :  return "Not set yet";
        /*case OPERATION      :  return "Operation";
        case LOGICAL        :  return "Logical";
        case EQUALS         :  return "Equals";
        case NOTEQUALS      :  return "Not Equals";
        case GREATERTHAN    :  return "Greater Than";
        case LESSTHAN       :  return "Less Than";
        case GREATEROREQUALS:  return "Greater or equals";
        case LESSOREQUALS   :  return "Less or equals";
        case SUM            :  return "Sum";
        case MAXIMUM        :  return "Maximum";
        case MINIMUM        :  return "Minimum";
        case OPERATOR       :  return "Operator";
        case TERM           :  return "Term";
        case ARITHMETIC     :  return "Arithmetic";
        case ADDITION       :  return "Addition";
        case SUBSTRACTION   :  return "Substraction";
        case MULTIPLICATION :  return "Multiplication";
        case DIVISION       :  return "Division";
        case EXPONENT       :  return "Exponent";
        case CONSTANT        : return "Constant";
        case VARIABLE        : return "Variable";*/
                                                            // Concept
        case CONCEPT         : return "Concept";
        case MODELCONCEPT    : return "Model Concept";
        case VISUALCONCEPT   : return "Visual Concept";
                                                            // Association
        case ASSOCIATION     : return "Association";
        case AGGREGATION     : return "Aggregation";
        case FROMASSOCIATION : return "From Association";
        case TOASSOCIATION   : return "To Association";
        case ASSOCIATABLE    : return "Associatable";
                                                            // Process
        case PROCESS         : return "Process";
                                                            // Task
        case TASK            : return "Task";
                                                            // Input
        case INPUT           : return "Input";
        case TASKINPUT       : return "Task Input";
        case KNOWLEDGEINPUT  : return "Knowledge Asset";
                                                            // Output
        case OUTPUT          : return "Output";
        case TASKOUTPUT      : return "Task Ouput";
        case KNOWLEDGEOUTPUT : return "Knowledge Output";
                                                            // Node
        case NODE            : return "Node";
        case INITIAL_NODE    : return "Initial Node";
        case FINAL_NODE      : return "Final Node";
        case DECISION        : return "Decision";
                                                            // Flow
        case FLOW            : return "Flow";
        case KNOWLEDGE_FLOW  : return "Knowledge Flow";
        case TASK_FLOW       : return "Task Flow";
                                                            // Activity
        case ACTIVITY        : return "Activity";
                                                            // Agent
        case AGENT           : return "Agent";
                                                            // KnowledgeAsset
        case KNOWLEDGEASSET  : return "Knowledge Asset";
                                                            // Element    
        case ELEMENT         : return "Element";
        case OWNABLEELEMENT  : return "Ownable Element";
        case MODELELEMENT    : return "Model Element";
                                                            // UI actions 
        case SELECT          : return "Select";
        case DELETE          : return "Delete";
        
        default: System.err.print("Get stateName ! Not implemented Yet");
        return "Get stateName ! Not implemented Yet"; 
        }
    }
}
