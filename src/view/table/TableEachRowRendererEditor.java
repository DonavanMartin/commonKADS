package view.table;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class TableEachRowRendererEditor extends JTable {
    private Class editingClass;
    private AbstractModelTable abstractModel;
    public TableEachRowRendererEditor(Object[][] data, String[] columnNames) {
        super(data, columnNames);
        abstractModel = new AbstractModelTable(columnNames[0],columnNames[1]); 
        super.setModel(abstractModel);
        super.setDefaultEditor(JComboBox.class, new CellEditor());
        super.setDefaultRenderer(String.class, new TextAreaRenderer());
        //super.setCellEditor(new CellEditor());
    }
    
    @Override
    public TableCellRenderer getCellRenderer(int row, int column) {
        editingClass = null;
        int modelColumn = convertColumnIndexToModel(column);
        if (modelColumn == 1) {
            Class rowClass = getModel().getValueAt(row, modelColumn).getClass();
            return getDefaultRenderer(rowClass);
        } else {
            return super.getCellRenderer(row, column);
        }
    }
    
    @Override
    public TableCellEditor getCellEditor(int row, int column) {
        editingClass = null;
        int modelColumn = convertColumnIndexToModel(column);
        if (modelColumn == 1) {
            editingClass = getModel().getValueAt(row, modelColumn).getClass();
            return getDefaultEditor(editingClass);
        } else {
            return super.getCellEditor(row, column);
        }
    }
    
    @Override
    public Class getColumnClass(int column) {
        return editingClass != null ? editingClass : super.getColumnClass(column);
    }
}