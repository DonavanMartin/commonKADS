package view.table;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class AbstractModelTable extends AbstractTableModel{
    private List<String> columnNames = new ArrayList();
    private List<Object[]> data      = new ArrayList();
    
    public AbstractModelTable(String column1, String column2){
        columnNames.add(column1);
        columnNames.add(column2);
    }
   
    @Override
    public void setValueAt(Object value, int row, int col) {
        data.get(row)[col] = value;
        fireTableCellUpdated(row, col);
    }

    public void addRow(Object [] o){    
        data.add(data.size(), o);
        fireTableRowsInserted(data.size() - 1, data.size() -1);
    }

    @Override
    public int getColumnCount(){
        return columnNames.size();
    }

    @Override
    public int getRowCount(){
        return data.size();
    }

    @Override
    public String getColumnName(int col){
        try{
            return columnNames.get(col);
        }
        catch(Exception e){
            return null;
        }
    }

    @Override
    public Object getValueAt(int row, int col){
        try{
            return data.get(row)[col];
        }catch(Exception e){
            return "";
        }
    }

    @Override
    public boolean isCellEditable(int row, int col){
        return (col != 0);
    }

    @Override
    public Class getColumnClass(int c){
        return getValueAt(0, c).getClass();
    }
    
    public void removeRow(int row) {
        data.remove(row);
        fireTableRowsDeleted(row, row);
    }
};