package view.table;

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

public class CellEditor extends AbstractCellEditor implements TableCellEditor {
    private TableCellEditor editor;
    
    public CellEditor(){
        super();
        editor = null;
    }
    
    @Override
    public Object getCellEditorValue() {
        if (editor != null) {
            return editor.getCellEditorValue();
        }
        return null;
    }
    
    @Override
    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int column) {
        if (value instanceof String) {
            editor = new DefaultCellEditor(new JTextField());
        } else if (value instanceof Boolean) {
            editor = new DefaultCellEditor(new JCheckBox());
        } else if (value instanceof JComboBox){
            editor = new DefaultCellEditor((JComboBox) value);
        }
        return editor.getTableCellEditorComponent(table, value, isSelected, row, column);
    }
    
}