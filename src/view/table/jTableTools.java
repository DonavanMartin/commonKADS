package view.table;

import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import view.element.Element;

public class jTableTools{
    
    public static void add_empty_row(JTable table){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.addRow(new Object[]{"", ""});
        updateRowHeights(table);
        setTableheight(table);
        setTextWrap(table);
    }
    
    public static void add_property_row(JTable table, String propertyName, Object o){
        AbstractModelTable model = (AbstractModelTable) table.getModel();
        if(o == null){
            model.addRow(new Object[]{propertyName, ""});
        }else{    
            if(o.getClass() == ArrayList.class){
                JComboBox comboBox = new JComboBox();
                try {
                    for(Element e : (ArrayList<Element>)o)
                        comboBox.addItem(e.getName());
                    comboBox.addItem("Item 1");
                    comboBox.addItem("Item 2");
                    comboBox.addItem("Item 3");
                    model.addRow(new Object[]{propertyName, comboBox});
                }catch(Exception e){
                    System.err.println("Error with multiple selection in Jtable");
                }
            }else{
                model.addRow(new Object[]{propertyName, o});
            }
        }
        //setTableheight(table);
        //setTextWrap(table);
        //table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        //table.getColumnModel().getColumn(0).setPreferredWidth(getMaximalRequiredColumnWidth(table,0));
    }
    
    public static void update(JTable table){
        updateRowHeights(table);
        setTableheight(table);
    }
    
    
    public static void setTableheight(JTable table){
        int width  = table.getWidth();
        int height = getRowsHeight(table);
        table.setPreferredSize(new Dimension(width, height));
    }
    
    public static void updateRowHeights(JTable table){
        for (int row = 0; row < table.getRowCount(); row++){
            int rowHeight = table.getRowHeight();
            for (int column = 0; column < table.getColumnCount(); column++){
                Component comp = table.prepareRenderer(table.getCellRenderer(row, column), row, column);
                rowHeight = Math.max(rowHeight, (int)comp.getPreferredSize().getHeight());
            }
            table.setRowHeight(row, rowHeight);
        }
    }
    
    private static int getRowsHeight(JTable table){
        int rowsHeight = 0;
        for (int row = 0; row < table.getRowCount(); row++){
            rowsHeight = rowsHeight + table.getRowHeight(row);
        }
        return rowsHeight;
    }
    
    private static void setTextWrap(JTable table){
        TableColumnModel cmodel = table.getColumnModel();
        TextAreaRenderer textAreaRenderer = new TextAreaRenderer();
        for(int col = 0; col<table.getColumnCount(); col++){
            cmodel.getColumn(col).setCellRenderer(textAreaRenderer);
        }
    }
    
    public static void remove_row(JTable table){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        
        int[] rows = table.getSelectedRows();
        for(int i=0;i<rows.length;i++){
            model.removeRow(rows[i]-i);
        }
        updateRowHeights(table);
        setTableheight(table);
    }
    
    public static void removeAllProperties(JTable table, String property){
        try{
            ArrayList<Integer> rowToDelete = new ArrayList<>();
            
            for(int i = 0; i < table.getModel().getRowCount(); i++){
                if(property.equals(table.getModel().getValueAt(i, 0))){
                    rowToDelete.add(i);
                };
            }
            for(int i=0;i<rowToDelete.size();i++){
                ((AbstractModelTable)table.getModel()).removeRow(rowToDelete.get(i)-i);
            }
            updateRowHeights(table);
            setTableheight(table);
        }catch(Exception e){}
    }
    
}