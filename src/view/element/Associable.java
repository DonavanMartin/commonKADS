package view.element;
import java.util.ArrayList;
import model.element.commonKADS.Decision;
import view.ui.Logs;

public abstract class Associable extends Element{

    public Associable(Logs log) {
        super(log);
    }
    
    public boolean addAssociation(Association association) {
        boolean dashed = association.isDashed();
        Associable associable = association.getFromAssociable();
        if(associable == this) {
            this.association.add(association);
            return true;
        }else{
            this.association.remove(association);
            return addToAssociation(association, dashed);
        }
    }
    
    private boolean addToAssociation(Association association, boolean dashed){
        Association realAssociation  = new From(dashed, logs);
        realAssociation.transferValues(association);
        //associable.addAssociation(realAssociation,dashed);
        this.association.add(realAssociation);
        return true;
    }
    
    public void resetAllAssociations(){
        association = new ArrayList<>();
    }
    
    public void removeAssociation(Association a){
        association.remove(a);
    }
    
    public void removeAssociation(Associable a){
        ArrayList<Association> toDelete = new ArrayList<>();
        
        for(Association b : association){
            if (b.getFromAssociable() == a){
                toDelete.add(b);
            }
            if(b.getToAssociable() == a){
                toDelete.add(b);
            }
        }
        for(Association d : toDelete){
            remove(d);
        }
    }
    private void remove(Association a){
        Associable from = a.getFromAssociable();
        Associable to   = a.getToAssociable();
        
        if(to instanceof Decision){
            to.removeInput(from);
        }else if(from instanceof Decision){
            from.removeOutput(to);
        }
        association.remove(a);
    }
    
    public void removeAssociation(){
        ArrayList<Associable> toA = new ArrayList<>();
        ArrayList<Associable> toB = new ArrayList<>();
        
        for(Association a : association){
            Associable b = a.getFromAssociable();
            toA.add(b);
            Associable c = a.getToAssociable();
            toB.add(c);
        }
        
        for(Associable d : toA){
            d.removeAssociation(this);
        }
        for(Associable d : toB){
            d.removeAssociation(this);
        }
        association = new ArrayList<>();
    }
    
    @Override
    public int getType(){ return type;};
    @Override
    public int getSuperType(){return superType;};
    
    public void addInput    (Element e){ logs.printError("addInput for"    + e.getClass().getName());}
    public void removeInput (Element e){ logs.printError("removeInput for" + e.getClass().getName());}
    public void addOutput   (Element e){ logs.printError("addOutput for"   + e.getClass().getName());}
    public void removeOutput(Element e){ logs.printError("removeOutput for"+ e.getClass().getName());}
    
}