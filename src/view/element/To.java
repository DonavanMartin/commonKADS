package view.element;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import app.Helper.ElementStates;
import view.ui.Logs;


public class To extends Association {
    
    private final int type = ElementStates.TOASSOCIATION;
    private int instance_of = -1;

    private final Polygon arrowHead;
    private final int LINE_LENGTH = 10;
    private final int ARROW_ANGLE = 30;
    private Logs log;

    public To(boolean dashed, Logs log) {
        super(dashed, log);
        arrowHead = new Polygon();
    }
    @Override
    public int getType() {
        return type;
    }
    
    public void setInstanceOf(int instance_type) {
        instance_of = instance_type;
    }
    
    public int getInstanceOf() {
        return instance_of;
    }
    
    private void computeArrowHead() {
        Point fromPoint = getFromPoint();
        Point toPoint = getToPoint();
        
        double theta = Math.toDegrees(Math.atan2(fromPoint.getY() - toPoint.getY(), toPoint.getX() - fromPoint.getX()));
        
        double toAngle1 = 90 - ARROW_ANGLE + theta;
        double toAngle2 = 90 - ARROW_ANGLE - theta;
        
        double dx1 = Math.sin(Math.toRadians(toAngle1))*LINE_LENGTH;
        double dy1 = Math.cos(Math.toRadians(toAngle1))*LINE_LENGTH;
        double dx2 = Math.sin(Math.toRadians(toAngle2))*LINE_LENGTH;
        double dy2 = Math.cos(Math.toRadians(toAngle2))*LINE_LENGTH;
        
        int x1 = (int)(toPoint.getX() - dx1);
        int y1 = (int)(toPoint.getY() - dy1);
        int x2 = (int)(toPoint.getX() - dx2);
        int y2 = (int)(toPoint.getY() + dy2);
        
        if(getLineLength() < LINE_LENGTH) {
            x1 = (int)(toPoint.getX());
            y1 = (int)(toPoint.getY());
            x2 = (int)(toPoint.getX());
            y2 = (int)(toPoint.getY());
        }
        
        arrowHead.reset();
        arrowHead.addPoint(x1, y1);
        arrowHead.addPoint((int)toPoint.getX(), (int)toPoint.getY());
        arrowHead.addPoint(x2, y2);
    }
    
    @Override
    public void draw(Graphics2D g2) {
        super.draw(g2);
        computeArrowHead();
        g2.fill(arrowHead);
    }
    
}
