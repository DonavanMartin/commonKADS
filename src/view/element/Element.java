package view.element;

import org.w3c.dom.Document;

import app.Helper.ElementStates;
import view.workSheets.BasicProperties;
import view.workSheets.WorkSheet;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import app.CommonKADSModel;
import java.util.Observable;
import view.shape.ElementShape;
import view.ui.Logs;

public abstract class Element extends Observable {
    protected CommonKADSModel superModel;
    protected ArrayList<WorkSheet> worksheets;
    protected ArrayList<Ownable> listOwnableElement;
    protected BasicProperties properties;

    protected int type;
    protected int superType;
    private   String id;
    protected String name;

    protected Element owner;
    protected ElementShape shape; 
   
    //protected ArrayList<Association> listAssociation;
    protected boolean highlighted;
    protected boolean hasNameConflict;
    protected boolean orphan; 
    protected boolean Incomplete;
    protected boolean isWarned;
    
    protected ArrayList<From> fromAssociation;
    protected ArrayList<To> toAssociation;
    protected ArrayList<To> pointerAssociation;
    protected ArrayList<Association> association;
         
    private int offsetX;
    private int offsetY;
    private final Font font;
    
    private boolean isSelected;
      
    public String typeName;
    protected Logs logs;
    
    public Element(Logs log) {
        this.logs =log;
        worksheets = new ArrayList<WorkSheet>();
        id = "";
        owner = null;
        name = "";

        font = new Font("SansSerif", Font.PLAIN, 14);
        type =-1;
        setTypeName(type);
        
        shape = null;
        fromAssociation     = new ArrayList<>();
        toAssociation       = new ArrayList<>();
        pointerAssociation  = new ArrayList<>();
        association         = new ArrayList<>();
  
        //shape = new ElementShape();
        
        // Element States
        highlighted   = false;
        isSelected    = false;
        isWarned      = false;
        listOwnableElement = new ArrayList<Ownable>();
        properties = new BasicProperties(this,log);
        worksheets.add(properties);
    }
    
    public ArrayList<Association> getListAssociation(){
        return association;
    }
    
    public BasicProperties getProperties() {
        return properties;
    }
    
    public void removeOwnableElement(Ownable ownableElement) {
        listOwnableElement.remove(ownableElement);
    }
    
    public int getListOwnableElementSize() {
        return listOwnableElement.size();
    }
    
    public Ownable getOwnableElement(int i) {
        return listOwnableElement.get(i);
    }
    
   public boolean isSmallerThan(Element e) {
        boolean isSmaller = true;
        if(e != null) {
            double thisArea = getBounds().getHeight() * getBounds().getWidth();
            double eArea = e.getBounds().getHeight() * e.getBounds().getWidth();
            if(!(thisArea < eArea))
                isSmaller = false;
        }
        return isSmaller;
    }
   
    public boolean contains(Element ownedOwnable) {
        boolean contains = false;
        if(shape.getContentShape().contains(ownedOwnable.getBounds()))
            contains = true;
        return contains;
    }

    public ArrayList<WorkSheet> getWorksheets() {
        return worksheets;
    }
   
    public Element getOwner() {
        return owner;
    }
    
    public void addOwnableElement(Ownable ownableElement) {
        listOwnableElement.add(ownableElement);
    }
    
    
    public void setSuperModel( CommonKADSModel m){
        superModel=m;
    }

    public boolean isHighlighted() {
        return highlighted;
    }
    public boolean isWarned() {
        return isWarned;
    }

    public boolean isHasNameConflict() {
        return hasNameConflict;
    }

    public boolean isOrphan() {
        return orphan;
    }

    public ArrayList<From> getFromAssociation() {
        return fromAssociation;
    }

    public ArrayList<To> getToAssociation() {
        return toAssociation;
    }

    public ArrayList<To> getPointerAssociation() {
        return pointerAssociation;
    }

    public ArrayList<Association> getTempAssociation() {
        return association;
    }
    
    
    public void setTypeName(int type){
        typeName =  ElementStates.getName(type);
    }
    
    public String getTypeName(){
        return ElementStates.getName(getType());
    }
    
    public int getType(){
        return type;
    }
       
    public int getSuperType(){
        return superType;
    }
    
    public int isOwnable(){
        return -1;
    }
    
    public Font getFont() {
        return font;
    }
    
    public void setLocation(Point point) {
        shape.setLocation(point);
    }
    
    public void setLocationWithOffsets(Point point) {
        int x = (int)point.getX() - offsetX;
        int y = (int)point.getY() - offsetY;
        if(x < 0) x = 0;
        if(y < 0) y = 0;
        setLocation(new Point(x, y));
        
        shape.setBounds((int)shape.getLocation().getX(), (int)shape.getLocation().getY());
        
        if(listOwnableElement.size() > 0){
            for (Ownable ownableElement : listOwnableElement){
                if(this != ownableElement){
                    ownableElement.setLocationWithOffsets(point);
                }
            }
        }
    }
    
    public void setMovingAgentLeftLocation(Point point) {
        setLocation(point);
        
        int x = (int)point.getX();
        int y = (int)point.getY();
        if(x < 0) x = 0;
        if(y < 0) y = 0;
        if(listOwnableElement.size() > 0){
            for (Ownable ownableElement : listOwnableElement){
                ownableElement.setLocationWithOffsets(point);
            }
        }
    }

    public void computeSwitchOffset(Point point) {
        offsetX = (int)getPosition().getX() - (int)point.getX();
        offsetY = (int)getPosition().getY() - (int)point.getY();
        
        if(listOwnableElement.size() > 0){
            for (Ownable ownableElement : listOwnableElement){
                ownableElement.computeDraggingOffset(point);
            }
        }
    }

    public int getOffsetX() {
        return offsetX;
    }

    
    
    public void setLocationAgentWithOffsets(Point point) {
        int x = (int)point.getX() - offsetX;
        int y = (int)point.getY();
        if(x < 0) x = 0;
        if(y < 0) y = 0;
        setLocation(new Point(x, y));
        
        shape.setBounds((int)shape.getLocation().getX(), (int)shape.getLocation().getY());
        
        if(listOwnableElement.size() > 0){
            for (Ownable ownableElement : listOwnableElement){
                if(this != ownableElement){
                    ownableElement.setLocationWithOffsets(point);
                }
            }
        }
    }
    
    public void computeDraggingOffset(Point point) {
        offsetX = (int)point.getX() - (int)getPosition().getX();
        offsetY = (int)point.getY() - (int)getPosition().getY();
        
        if(listOwnableElement.size() > 0){
            for (Ownable ownableElement : listOwnableElement ){
                if(this != ownableElement){
                    ownableElement.computeDraggingOffset(point);
                }
            }
        }
    }

    public boolean isIncomplete() {
        return Incomplete;
    }

    public void setIsIncomplete(boolean isIncomplete) {
        this.Incomplete = isIncomplete;
    }
    
    public boolean isSelected() {
        return isSelected;
    }
    
    public void setIsSelected(boolean s) {
        isSelected = s;
    }
    
    public int getNameWidth() {
        return shape.getNameWidth();
    }
    
    public void setNameWidth(int nw) {
        shape.setNameWidth(nw); 
    }
    
    public int getWidth() {
        return shape.getWidth();
    }
    
    public void setWidth(int w) {
        shape.setWidth(w);
    }
    
    public int getContentHeight() {
        return shape.getContentHeight();
    }
    
    public void setContentHeight(int ch) {
        shape.setContentHeight(ch);
    }
    
    public boolean isInSelectionZone(Point point){
        return shape.isInSelectionZone(point);
    }
    
    public Rectangle getBounds(){
        return shape.getBounds();
    }

    public void activateOwnerHighlight() {
        highlighted = true;
    }
    
    // Observable : On change value listener 
    // To update UI when name change from table
    public synchronized String getName(){
        return name;
    }

    public void setName(String newName) {
        synchronized (this) {
            String part1 = newName.substring(0, 1).toUpperCase();
            String part2 = newName.substring(1);
            newName = part1 + part2;
            name = newName;
            setNameWidth(name.length()*9);
            
        }
        setChanged();
        notifyObservers();
    }
    
    public void setName(String newName, int elementNumber) {
        String nameVerified = nameConflict(newName, elementNumber);
        String part1 = nameVerified.substring(0, 1).toUpperCase();
        String part2 = nameVerified.substring(1);
        nameVerified = part1 + part2;
        name = nameVerified;
        setNameWidth(name.length()*9);
    }
    
    private String nameConflict(String name, int elementNumber) {
        name = name.trim();
        boolean inConflict = false;
        for(int i = 0; i < superModel.getListVisualElementSize(); i++) {
            Element elementIt = (Element) superModel.getVisualElement(i);
            String tempName = elementIt.getName().trim();
            if(tempName.contains("("))
                tempName = tempName.substring(0, tempName.indexOf('('));
            if(tempName.equalsIgnoreCase(name) && elementIt != this)
                inConflict = true;
        }
        if(inConflict) {
            if(name.contains("_"))
                name = name.substring(0, name.indexOf('_'));
            name += "_" + elementNumber++;
        }
        return name;
    }   
    
    public String getId() {
        return id;
    }
    
    public void setId(String newId) {
        id = newId;
    }
    
    public void setOwner(Element newOwner) {
        owner = newOwner;
        updateWarning();
    }
    
    public void updateWarning(){
    if(    hasNameConflict 
        || orphan
        || Incomplete
        || owner == null){
            isWarned = true;
        }else{
            isWarned = false;
        }
    }
    
    public void draw(Graphics2D g2) {
       shape.draw(g2,this);
    } 
    
    public Point getPosition() {
        return shape.getLocation();
    } 
    
    public void activateContainedHighlight() {
        highlighted = true;
    }
    
    public void resetHighlight() {
        highlighted = false;
    }
    
    public abstract Element writeXML(Document xmlDoc);
    
    public void setBounds(int x, int y){
        shape.setBounds(x, y);
    }
    
    public void setIsResizing(boolean b){
        shape.setIsResizing(b);
    }
    public void resize(Point point){
        shape.resize(point);
    }    
 
    public void setSelectedHandle(Point point){
        shape.setSelectedHandle(point);
    }
    
    public void unsetSelectedHandle(){
        shape.unsetSelectedHandle();
    }             
}
