package view.element;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Line2D;

import app.Helper.ElementStates;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import model.element.commonKADS.Decision;
import view.ui.Logs;
import view.workSheets.BasicProperties;
import view.workSheets.Decision_worksheet;
import view.workSheets.WorkSheet;

public class Association {
    private final int type = ElementStates.ASSOCIATION;
    protected ArrayList<WorkSheet> worksheets;
    private Font font;

    private String name;
    private Associable from;
    private Associable to;
    private Point fromPoint;
    private Point toPoint;
    private Line2D.Double line;
    private Polygon area;
    private final int NB_POINTS = 4;
    private int[] xPoints;
    private int[] yPoints;
    private final int AREA_WIDTH = 5;
    private boolean dashed;
    private BasicStroke stroke;
    protected Logs logs;
    protected BasicProperties properties;
    
    public Association(boolean dashed, Logs log) {
        worksheets = new ArrayList<WorkSheet>();
        font = new Font("SansSerif", Font.PLAIN, 14);

        name ="";

        this.dashed = dashed;
        if(dashed){
            float dash1[] = {10.0f};
            stroke = new BasicStroke(1.0f,
                        BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER,
                        10.0f, 
                        dash1, 
                        0.0f);
        }else{
            stroke = new BasicStroke();
        }
        
        to = null;
        fromPoint = new Point();
        toPoint = new Point();
        line = new Line2D.Double();
        xPoints = new int[NB_POINTS];
        yPoints = new int[NB_POINTS];
        area = new Polygon();
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public BasicProperties getProperties() {
        return properties;
    }
    public ArrayList<WorkSheet> getWorksheets() {
        return worksheets;
    }
   
    public boolean isDashed() {
        return dashed;
    }
    
    public void transferValues(Association association) {
        from = association.from;
        to = association.to;
        fromPoint = association.fromPoint;
        toPoint = association.toPoint;
        line = association.line;
        xPoints = association.xPoints;
        yPoints = association.yPoints;
        area = association.area;
    }
    
    public int getType() {
        return type;
    }
    
    public Point getFromPoint() {
        return fromPoint;
    }
    
    public Point getToPoint() {
        return toPoint;
    }
    
    public void setToPoint(Point point) {
        toPoint = point;
        fromPoint = computeDependantPoint(from, toPoint);
        line.setLine(fromPoint, toPoint);
    }
    
    public void setFrom(Associable associatable) {
        from = associatable;
        fromPoint = computeDependantPoint(from, toPoint);
        line.setLine(fromPoint, toPoint);
        computeAssociationArea();
    }
    
    public void setTo(Associable associable) {
        to = associable;
        toPoint = computeDependantPoint(to, fromPoint);
        fromPoint = computeDependantPoint(from, toPoint);
        line.setLine(fromPoint, toPoint);
        computeAssociationArea();
    }
    public void removeAssociable() {
        to = null;
        fromPoint = new Point();
        toPoint = new Point();
        line = new Line2D.Double();
        xPoints = new int[NB_POINTS];
        yPoints = new int[NB_POINTS];
        area = new Polygon();
    }
   
    
    public Associable getFromAssociable() {
        return from;
    }
    
    public Associable getToAssociable() {
        return to;
    }
    
    public boolean contains(Point point) {
        return area.contains(point);
    }
    
    public int getLineLength() {
        return (int)(Math.sqrt(Math.pow(toPoint.getX() - fromPoint.getX(), 2) + Math.pow(toPoint.getY() - fromPoint.getY(), 2)));
    }
    
    public void removeAtOtherEnd(Associable associable) {
        if(from != null){
            if(from == associable){
                to.removeAssociation(this);
            }
        }
        if(to != null){
            if(to == associable){
                from.removeAssociation(this);
            }
        }
    }
    
    public void move(Element movingAio) {
        if(to == null){
            fromPoint = computeDependantPoint(from, toPoint);
            
        }else if(movingAio == from){
            fromPoint = computeDependantPoint(from, toPoint);
            toPoint = computeDependantPoint(to, fromPoint);
            
        }else if(movingAio == to) {
            toPoint = computeDependantPoint(to, fromPoint);
            fromPoint = computeDependantPoint(from, toPoint);
        }
        
        line.setLine(fromPoint, toPoint);
        computeAssociationArea();
    }
    
    private Point computeDependantPoint(Associable from, Point fixedPoint) {
        Point dependantPoint = new Point();
        Rectangle fromBounds = ((Element) from).getBounds();
        
        int fixedX = (int)fixedPoint.getX();
        int leftLimit = (int)fromBounds.getMinX();
        int rightLimit = (int)fromBounds.getMaxX();
        
        if(fixedX < leftLimit)
            dependantPoint.setLocation(leftLimit, dependantPoint.getY());
        else if(fixedX > rightLimit)
            dependantPoint.setLocation(rightLimit, dependantPoint.getY());
        else
            dependantPoint.setLocation(fixedX, dependantPoint.getY());
        
        // compute shortest y
        int toY = (int)fixedPoint.getY();
        int upperLimit = (int)fromBounds.getMinY();
        int lowerLimit = (int)fromBounds.getMaxY();
        if(toY < upperLimit)
            dependantPoint.setLocation(dependantPoint.getX(), upperLimit);
        else if(toY > lowerLimit)
            dependantPoint.setLocation(dependantPoint.getX(), lowerLimit);
        else
            dependantPoint.setLocation(dependantPoint.getX(), toY);
        
        return dependantPoint;
    }
    
    private void computeAssociationArea() {
        double bigOpposite = toPoint.getX() - fromPoint.getX();
        double bigAdjacent = toPoint.getY() - fromPoint.getY();
        int smallHypotenuse = AREA_WIDTH/2;
        double angle = 90 - Math.toDegrees(Math.atan(bigOpposite/bigAdjacent));
        double dx = Math.sin(Math.toRadians(angle))*smallHypotenuse;
        double dy = Math.cos(Math.toRadians(angle))*smallHypotenuse;
        
        // corner A
        xPoints[0] = (int) (fromPoint.getX() + dx);
        yPoints[0] = (int) (fromPoint.getY() - dy);
        
        // corner B
        xPoints[1] = (int) (fromPoint.getX() - dx);
        yPoints[1] = (int) (fromPoint.getY() + dy);
        
        
        // corner C
        xPoints[2] = (int) (toPoint.getX() - dx);
        yPoints[2] = (int) (toPoint.getY() + dy);
        
        // corner D
        xPoints[3] = (int) (toPoint.getX() + dx);;
        yPoints[3] = (int) (toPoint.getY() - dy);
        
        area = new Polygon(xPoints, yPoints, NB_POINTS);
    }

    public Font getFont() {
        return font;
    }
    
    public void draw(Graphics2D g2) {
        g2.setStroke(stroke);
        g2.setColor(Color.BLACK);
        g2.draw(line);
        
        

        g2.setFont(font);
        FontMetrics fm = g2.getFontMetrics(getFont());
        double width = fm.stringWidth(name);
        double offset = width /2;
        double x = (line.x2 + line.x1)/2;
        x = x -offset;
        double y = (line.y2 + line.y1)/2;
        g2.drawString(name,
                        (int)(x+1),
                        (int)(y-4));
        
    }
    
    public Line2D.Double getLine(){
        return line;
    };
    
    
}
