package view.element;

import java.awt.Point;
import java.awt.Rectangle;
import app.Helper.ElementStates;

public interface Ownable{
    public final int isOwnable = ElementStates.OWNABLEELEMENT;
    Element getOwnableElement();
    void computeDraggingOffset(Point point);
    void setLocationWithOffsets(Point point);
    void activateContainedHighlight();
    void resetHighlight();
    void setOwner(Element element);
    public abstract void addOwnableElement(Ownable ownableElement);
    public int isOwnable();
}
