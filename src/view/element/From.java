package view.element;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;
import app.Helper.ElementStates;
import view.ui.Logs;

public class From extends Association {
    private final int type = ElementStates.FROMASSOCIATION;
    private int instance_of = -1;
    private Line2D.Double line1;
    private Line2D.Double line2;
    private Point p1;
    private Point p2;
    private final int LINE_LENGTH = 10;
    private final int ARROW_ANGLE = 30;
    private Logs log;
    
    
    public From(boolean dashed, Logs log) {
        super(dashed , log);
        line1 = new Line2D.Double();
        line2 = new Line2D.Double();
        p1 = new Point();
        p2 = new Point();
    }
    
    @Override
    public int getType() {
        return type;
    }
    
    public void setInstanceOf(int instance_type) {
        instance_of = instance_type;
    }
    
    public int getInstanceOf() {
        return instance_of;
    }
    
    private void computeArrowHead() {
        Point fromPoint = getFromPoint();
        Point toPoint = getToPoint();
        
        double theta = Math.toDegrees(Math.atan2(fromPoint.getY() - toPoint.getY(), toPoint.getX() - fromPoint.getX()));
        
        double toAngle1 = 90 - ARROW_ANGLE + theta;
        double toAngle2 = 90 - ARROW_ANGLE - theta;
        
        double dx1 = Math.sin(Math.toRadians(toAngle1))*LINE_LENGTH;
        double dy1 = Math.cos(Math.toRadians(toAngle1))*LINE_LENGTH;
        double dx2 = Math.sin(Math.toRadians(toAngle2))*LINE_LENGTH;
        double dy2 = Math.cos(Math.toRadians(toAngle2))*LINE_LENGTH;
        
        int x1 = (int)(toPoint.getX() - dx1);
        int y1 = (int)(toPoint.getY() - dy1);
        int x2 = (int)(toPoint.getX() - dx2);
        int y2 = (int)(toPoint.getY() + dy2);
        
        if(getLineLength() < LINE_LENGTH) {
            x1 = (int)(toPoint.getX());
            y1 = (int)(toPoint.getY());
            x2 = (int)(toPoint.getX());
            y2 = (int)(toPoint.getY());
        }
        
        p1.setLocation(x1, y1);
        p2.setLocation(x2, y2);
        
        line1.setLine(p1, toPoint);
        line2.setLine(p2, toPoint);
    }
    
    @Override
    public void draw(Graphics2D g2) {
        super.draw(g2);
        computeArrowHead();
        g2.draw(line1);
        g2.draw(line2);
    }
    
}
