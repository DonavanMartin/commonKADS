package view.element;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

public class Corner {
    
    private Rectangle handle;
    private Point location;
    private final int SIZE = 9;
    
    public Corner() {
        handle = null;
        location = new Point(0,0);
    }
    
    public void setLocation(Point point) {   
        location = point;
    }
    
    public Point getLocation() {
        return location;
    }
    
    public boolean contains(Point point) {
        boolean contains = false;
        if(handle.contains(point))
            contains = true;
        return contains;
    }
    
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        handle = new Rectangle((int)location.getX()-SIZE/2, (int)location.getY()-SIZE/2, SIZE, SIZE);
        g2.setColor(Color.BLACK);
        g2.fill(handle);
    }
    
}
