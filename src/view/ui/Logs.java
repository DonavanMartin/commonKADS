package view.ui;

import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Logs extends JScrollPane {
    private static final DateFormat logTimestampFormat = new SimpleDateFormat("HH:mm:ss");
    private JTextArea messageArea = null;

    public Logs() {
        messageArea = new JTextArea();
        messageArea.setEditable(false);
        messageArea.setFont(new Font("Courier New", Font.PLAIN, 12));
        setViewportView(messageArea);
    }

    private void println(String s) {
        messageArea.append(s);
        JScrollBar sb = ((JScrollPane) messageArea.getParent().getParent()).getVerticalScrollBar();
        
        if (null != sb)
            sb.setValue(sb.getMaximum());
        messageArea.append("\n");
    }

    public void printInfo(String s) {
        StringBuilder b = new StringBuilder();
        b.append("[INFO](");
        b.append(logTimestampFormat.format(new Date()));
        b.append("): ");
        b.append(s);
        println(b.toString());
    }
    

    public void printError(String s) {
        StringBuilder b = new StringBuilder();
        b.append("[ERR ](");
        b.append(logTimestampFormat.format(new Date()));
        b.append("): ");
        b.append(s);
        println(b.toString());
    }
    
    public void printSuccess(String s) {
        StringBuilder b = new StringBuilder();
        b.append("[SUCC](");
        b.append(logTimestampFormat.format(new Date()));
        b.append("): ");
        b.append(s);
        println(b.toString());
    }
       
}
