package view.ui;

import controller.Control;
import controller.FileControl;
import controller.PropertiesChange;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class mainPanel extends JPanel{
    private GridBagConstraints gbc;
    private CanvasCommonKADS canvas;

    private Control control;
    private JButton addFileButton;
    private FileControl fileController;
    private Logs log;

    public mainPanel(FileControl fileController, Logs log){
        this.log = log;
        this.fileController = fileController;
        control = new Control(fileController, log);
        canvas = new CanvasCommonKADS(control);
        control.setCanvas(canvas);
        
        addFileButton = canvas.getToolBar().addFileButton(fileController);
        
        setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
        setHierchy();
        setToolBar();
        setModelingArea();
        setBottomPanel();
        setVisible(true);
        
        log.printInfo("Panel Created");
    }
    private void setHierchy(){
        gbc.gridx = 0;
        gbc.gridy  = 0;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        //gbc.weightx = GridBagConstraints.1;
        //gbc.weighty = GridBagConstraints.RELATIVE;
        gbc.gridheight = 2;
        add(addFileButton, gbc);
    
    }
    private void setToolBar() {
        setGridBagConstraints(1,0,0,0);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.fill = GridBagConstraints.VERTICAL;
        //gbc.weighty = 3;
        gbc.gridheight =2;
        add(canvas.getToolBar(), gbc);
    }
    
    private void setModelingArea() {
        setGridBagConstraints(2,0,1,1);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.fill = GridBagConstraints.BOTH;
        add(canvas, gbc);
    }
    
    private void setBottomPanel() {
        setGridBagConstraints(2,1,1,1);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.fill = GridBagConstraints.BOTH;
        add(control.getPropertiesController().getBottomPanel().getTabbedPanel(), gbc);
    }
    
    private void setGridBagConstraints(int gridx, int gridy, int gridwidth, int gridheight){
        gbc.gridx = gridx;
        gbc.gridy  = gridy;
        gbc.weightx = gridwidth;
        gbc.weighty = gridheight;
        gbc.gridwidth = GridBagConstraints.RELATIVE;
        gbc.gridheight = GridBagConstraints.RELATIVE;
    }
    
    public CanvasCommonKADS getCanvas(){
        return canvas;
    } 
    
}