package view.ui;

import controller.Control;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

public class CanvasCommonKADS extends JPanel{
    private Canvas canvas;
    private ToolBar toolBar;
    private TitledBorder titledBorder;;
    private final int HEIGHT = 600;
    private final int WIDTH = 1000;
    private final int MARGIN = 10;

    private Control control;    
    private final String TITLE = "Model";
    
    public CanvasCommonKADS(Control control) {
        this.control = control;
        setLayout(new BorderLayout());
        
        titledBorder = BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), TITLE);
        titledBorder.setTitleJustification(TitledBorder.CENTER);
        setBorder(titledBorder);
        
        canvas = new Canvas();
        canvas.setOpaque(true);
        canvas.setBackground(Color.WHITE);
        
        JScrollPane scrollPane = new JScrollPane(canvas);
        scrollPane.setAutoscrolls(true);
        
        setMinimumSize(new Dimension(WIDTH, HEIGHT));
        setPreferredSize(new Dimension(WIDTH, HEIGHT));

        add(scrollPane);
        
        toolBar = new ToolBar();
        toolBar.setCanvasController(control.getCanvasController());
        // Select initial file
        canvas.addMouseListener(control.getListener());
        canvas.addMouseMotionListener(control.getListener());
    }
    
    public void setDebugTitle(Point p){
        titledBorder.setTitle(TITLE + " x:"+ p.getX()+" y:"+ p.getY());
    }
    public ToolBar getToolBar() {
        return toolBar;
    }
    
    public void update() {
        Dimension coveredArea = control.getModel().getBounds();
        canvas.setPreferredSize(new Dimension(coveredArea.width+MARGIN, coveredArea.height+MARGIN));
        canvas.revalidate();
        repaint();
    }
    
    public class Canvas extends JPanel {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            if(control.getModel() != null) {
                control.getModel().draw(g2);
            }
        }
    }
}