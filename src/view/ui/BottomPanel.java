package view.ui;

import view.workSheets.OM3;
import view.workSheets.WorkSheet;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import view.table.TableEachRowRendererEditor;


public class BottomPanel {
    private Dimension minDimension = new Dimension(400,200);
    
    private JTextArea textLog;
    private JButton ClearLogButton;
    private JTabbedPane bottomPanel;
    private JPanel logTab;
    private JPanel propertiesPanel;
    private JScrollPane propertiesTab;


    private JPanel warningPanel;
    private JScrollPane warningTab;
    private TableEachRowRendererEditor warningTable;
    private JScrollPane warningTabScroll;
    private ArrayList<WorkSheet> arrayWorkSheet;
    private GridBagConstraints gridBagConstraints;

    public BottomPanel() {  
        arrayWorkSheet  = new ArrayList<WorkSheet>();
        setPropertiesTab();
        setTabbedPane();
        setWarningTab();
        setLogsTab();
    }
    
    private void setPropertiesTab(){
        propertiesPanel = new javax.swing.JPanel();
        propertiesPanel.setLayout(new java.awt.GridBagLayout());
        
        propertiesTab = new javax.swing.JScrollPane();
        propertiesTab.setViewportView(propertiesPanel);
        propertiesTab.getAccessibleContext().setAccessibleName("PropertiesPanel");
    }
    
    private void setTabbedPane(){
        bottomPanel = new javax.swing.JTabbedPane();
        bottomPanel.addTab("Properties", propertiesTab);
        bottomPanel.getAccessibleContext().setAccessibleName("WarningPanel");
        bottomPanel.setMinimumSize(minDimension);
        bottomPanel.setPreferredSize(minDimension);
    }
    
    private void setLogsTab(){
        logTab = new javax.swing.JPanel();
        java.awt.GridBagLayout LogScrollPanelLayout = new java.awt.GridBagLayout();
        LogScrollPanelLayout.rowWeights = new double[] {5.0};
        logTab.setLayout(LogScrollPanelLayout);

        gridBagConstraints              = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx        = 0;
        gridBagConstraints.gridy        = 0;
        gridBagConstraints.gridwidth    = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.gridheight   = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.fill         = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx      = 1.0;
        gridBagConstraints.weighty      = 1.0;

        ClearLogButton = new javax.swing.JButton();
        ClearLogButton.setText("Clear");
        ClearLogButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textLog.setText("");
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        logTab.add(ClearLogButton, gridBagConstraints);
        ClearLogButton.getAccessibleContext().setAccessibleName("ClearLogButton");

        bottomPanel.addTab("Log", logTab);
        logTab.getAccessibleContext().setAccessibleName("logTab");
    }
  
    private void setWarningTab(){
        warningTab       = new javax.swing.JScrollPane();
        warningTabScroll = new javax.swing.JScrollPane();
        warningPanel     = new javax.swing.JPanel();
        warningTable     = new TableEachRowRendererEditor(
                new Object [][] {},
                new String [] {"Property", "Value"}
        );

        warningPanel.setLayout(new java.awt.BorderLayout());
        
        warningTabScroll.setViewportView(warningTable);
        warningPanel.add(warningTabScroll, java.awt.BorderLayout.CENTER);
        warningTab.setViewportView(warningPanel);
        bottomPanel.addTab("Warnings", warningTab);
        warningTab.getAccessibleContext().setAccessibleName("WarningPanel");
    
    }
   
    public void addLogsPanel(JScrollPane jsp){
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 5.0;
        logTab.add(jsp, gridBagConstraints);
        textLog = (JTextArea)jsp.getViewport().getView();
    }
    
    public void addWorkSheet(WorkSheet w){
        gridBagConstraints = new java.awt.GridBagConstraints();
       
        gridBagConstraints.gridx        = 0;
        gridBagConstraints.gridy        = arrayWorkSheet.size();
        gridBagConstraints.gridwidth    = 1;
        gridBagConstraints.gridheight   = 1;
        gridBagConstraints.fill         = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor       = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx      = 1.0;
        gridBagConstraints.weighty      = 1.0;
        propertiesPanel.add(w, gridBagConstraints);
        int height = propertiesPanel.getPreferredSize().height;
        propertiesPanel.setPreferredSize(new Dimension(0, height+100));

        arrayWorkSheet.add(w);
    }
    
    public void removeWorksheets(){
        propertiesPanel.removeAll();
        propertiesPanel.setPreferredSize(new Dimension(0, 0));
        this.arrayWorkSheet.clear();
    }
    
    public JTabbedPane getTabbedPanel(){
        return bottomPanel;
    }

    public JScrollPane getWarningTab() {
        return warningTab;
    }

    public JPanel getLogTab() {
        return logTab;
    }

    public JScrollPane getPropertiesTab() {
        return propertiesTab;
    }
    
    
    
    /*
    public WorkSheet getWorkSheet(String name){
        boolean unique = true;
        WorkSheet ret = null;
        
        for(WorkSheet w : arrayWorkSheet){
            if (w.getName().equals(name)){
                if(unique == true){
                  ret = w;              
                }else{
                    // PRINT ERROR ! 
                }
            }
        }
        return ret;
    }
    */

    public TableEachRowRendererEditor getWarningTable() {
        return warningTable;
    }
}
