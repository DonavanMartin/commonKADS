package view.ui;

import app.Helper.ElementStates;
import controller.CanvasControl;
import controller.FileControl;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.TitledBorder;
import javax.swing.JButton;

public class ToolBar extends JPanel {
    private Logs log;
    private CanvasControl canvasControler;
    private final String TITLE = "Tools";
    private Dimension dimensionMax;
    
    private JToggleButton bAgent;
    private JToggleButton bTask;
    private JToggleButton bKnowledgeAsset;
    private JToggleButton bInitialNode;
    private JToggleButton bFinalNode;
    private JToggleButton bDecision;
    private JToggleButton bflow;
    private JToggleButton bSelect;
    private JToggleButton bDelete;
    //private ImagedComboBoxProcessTool comboBox;
    
    private final static int nbButon = 8;
   
    
    private ButtonGroup paletteButtons;
    
    private GridBagConstraints gbc;
    
    public ToolBar() {
        dimensionMax = new Dimension(50, 40);
        TitledBorder titledBorder;
        titledBorder = BorderFactory.createTitledBorder(null, TITLE, TitledBorder.CENTER, TitledBorder.ABOVE_TOP);
        titledBorder.setTitleJustification(TitledBorder.CENTER);
        setBorder(titledBorder);
        
        setMinimumSize(dimensionMax);
        
        createToolBarPanel();
        canvasControler = null;
        
        // Layout
        setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = GridBagConstraints.RELATIVE;
        gbc.insets = new Insets(0, nbButon, 0, nbButon);
        add(bAgent,     gbc);
        add(bTask,     gbc);
        add(bKnowledgeAsset,   gbc);
        add(bInitialNode,    gbc);
        add(bFinalNode, gbc);
        add(bDecision, gbc);
        add(bflow,    gbc);
        add(bSelect,      gbc);
        add(bDelete,      gbc);
        //add(comboBox,     gbc);
    }
    
    public void setCanvasController(CanvasControl cc) {
        canvasControler = cc;
        //comboBox.setCanvasController(cc);
    }
    
    private void createToolBarPanel() {
        // buttons
        bAgent          = createPaletteButton("/view/design/64_32/agent.gif",      "Agent",           ElementStates.AGENT);
        bTask           = createPaletteButton("/view/design/64_32/task.gif",       "Task",            ElementStates.TASK);
        bKnowledgeAsset = createPaletteButton("/view/design/64_32/KA.gif",         "KnownledgeAsset", ElementStates.KNOWLEDGEASSET);
        bInitialNode    = createPaletteButton("/view/design/64_32/start.gif",      "InitialNode",     ElementStates.INITIAL_NODE);
        bFinalNode      = createPaletteButton("/view/design/64_32/end.gif",        "FinalNode",       ElementStates.FINAL_NODE);
        bDecision       = createPaletteButton("/view/design/64_32/decision.gif",   "Decision",        ElementStates.DECISION);

        bflow           = createPaletteButton("/view/design/64_32/flow.gif",       "Flow",            ElementStates.FLOW);
        bDelete         = createPaletteButton("/view/design/delete.gif",           "Delete",          ElementStates.DELETE);
        bSelect         = createPaletteButton("/view/design/select.gif",           "Select",          ElementStates.SELECT);
        bSelect.setSelected(true);
        
        /* palette buttons
        paletteButtons = new ButtonGroup();
        paletteButtons.add(bTask);
        paletteButtons.add(bKnowledgeAsset);
        paletteButtons.add(bInitialNode);
        paletteButtons.add(bFinalNode);
        paletteButtons.add(bTaskflow);
        paletteButtons.add(bKAflow);
        paletteButtons.add(bSelect);
        paletteButtons.add(bDelete);
        
        // ComboBox
        comboBox = new ProcessTool();*/

   
    }
    
    private JToggleButton createPaletteButton(String imagePath, String toolTip, int modelCanvasState){
        JToggleButton button = new JToggleButton();
        URL imageUrl = this.getClass().getResource(imagePath);
        button.setBackground(Color.WHITE);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setAlignmentY(Component.CENTER_ALIGNMENT);
        button.setIcon(new ImageIcon(imageUrl));
        button.setToolTipText(toolTip);
        button.addActionListener(new paletteClickListener(modelCanvasState));
        return button;
    }
    
    
    public JButton addFileButton(FileControl fileController) {
        JButton button = new JButton();
        button.setMargin(new Insets(0, 0, 0, 0));
        
        URL imageUrl = this.getClass().getResource("/view/design/open.png");
        button.setIcon(new ImageIcon(imageUrl));
        
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        
        button.setMaximumSize(dimensionMax);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setAlignmentY(Component.CENTER_ALIGNMENT);
        button.setToolTipText("Change file");
        button.addActionListener(new addFileClickListener(fileController));
        return button;
    }

    public void setLogs(Logs log) {
        this.log = log;
    }
    
    private class paletteClickListener implements ActionListener {
        private final int toolBarControlerState;
        public paletteClickListener(int state){
            toolBarControlerState = state;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            canvasControler.setCanvasState(toolBarControlerState);
        }
    }
    
    private class addFileClickListener implements ActionListener {
        private final FileControl fileController;
        public addFileClickListener(FileControl fileController){
            this.fileController = fileController;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            //fileController.saveFile();
            fileController.selectFileDialog();
        }
    }
}
