package view.design;

import java.awt.Color;

/**
 * SOURCE : http://www.rapidtables.com/web/color/RGB_Color.htm
 */
public class ColorPalette {
    
    public static final Color REDLIGHT     = new Color(255, 204, 204);
    public static final Color ORANGELIGHT  = new Color(255, 229, 204);
    public static final Color YELLOWLIGHT  = new Color(255, 255, 204);
    public static final Color GREENLIGHT   = new Color(229, 255, 204);
    public static final Color LIMELIGHT    = new Color(204, 255, 204);
    public static final Color AQUALIGHT    = new Color(204, 255, 229);
    public static final Color CYANLIGHT    = new Color(204, 255, 255);
    public static final Color BLUELIGHT    = new Color(204, 229, 255);
    public static final Color VIOLETLIGHT  = new Color(204, 204, 255);
    public static final Color MAGENTALIGHT = new Color(229, 204, 255);
    public static final Color PINKLIGHT    = new Color(255, 204, 255);
    public static final Color PURPLELIGHT  = new Color(255, 204, 229);
    public static final Color WHITE        = new Color(255, 255, 255);
}
