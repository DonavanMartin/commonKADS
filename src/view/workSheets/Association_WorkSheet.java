package view.workSheets;

import view.table.AbstractModelTable;
import view.table.TableEachRowRendererEditor;
import static view.table.jTableTools.add_property_row;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import view.element.Element;
import model.element.commonKADS.Task;
import view.element.Association;
import view.ui.Logs;

public class Association_WorkSheet extends WorkSheet{
    private int no;
    
    // Dependency and flow
    private String condition;
    private Task inputTasks;
    private ArrayList<Task> outputsTasks;

    public Association_WorkSheet(Association a, Logs log) {
        super(a, "First analysis of the application-assessment task TM-1", log);
        association = a;
        no = 0;
        name = "";
        condition = "[]";
        
        inputTasks       = null;
        outputsTasks      = new ArrayList<>();
        initRows();
        addValueChangeListener();
    }

    public String getCondition() {
        return condition;
    }
    
    
    public void initRows(){
        add_property_row(table, "no"                        ,no);
        add_property_row(table, "name"                      ,name);
        add_property_row(table, "condition"                 ,condition);
        add_property_row(table, "dependency tasks"          ,inputTasks);
        add_property_row(table, "flow tasks"                ,outputsTasks);
    }
    
    public void addValueChangeListener(){
        table.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("tableCellEditor".equals(evt.getPropertyName())) {
                    if (table.isEditing()){
                        processEditingStarted();
                    }else{
                        processEditingStopped(evt);
                    }
                }
            }
            private void processEditingStopped(PropertyChangeEvent evt) {
                TableEachRowRendererEditor t = (TableEachRowRendererEditor)evt.getSource();
                String editingProperty = ((AbstractModelTable)t.getModel()).getValueAt(table.getEditingRow(), 0).toString();
                switch (editingProperty){
                    case "no"                        :
                    case "name"                      : association.setName((String) table.getModel().getValueAt(table.getEditingRow(), table.getEditingColumn()));
                    case "condition"                 : condition = ((String) table.getModel().getValueAt(table.getEditingRow(), table.getEditingColumn()));

                    default : log.printError("Property Chaged:" +  evt.getPropertyName() + "Not Set Yet for TM1");
                    
                }
            }
            
            private void processEditingStarted() {
                
            }
        });
    }
    
    public int getNo() {
        return no;
    }
    
    public void setNo(int no) {
        this.no = no;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Task getInputTasks() {
        return inputTasks;
    }
    
    public void setInputsTasks(Task inputTasks) {
        this.inputTasks = inputTasks;
    }
    
    public ArrayList<Task> getOutputsTasks() {
        return outputsTasks;
    }
    
    public void setOutputsTasks(ArrayList<Task> outputsTasks) {
        this.outputsTasks = outputsTasks;
    }
    
    // Add
    public void addOutputsTasks(Task t){
        outputsTasks.add(t);
    };
    public void removeOutputsTasks(Task t){
        outputsTasks.remove(t);
    };
    
    
}
