package view.workSheets;

import view.table.AbstractModelTable;
import view.table.TableEachRowRendererEditor;
import static view.table.jTableTools.add_property_row;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import view.element.Element;
import model.element.commonKADS.Agent;
import model.element.commonKADS.KnowledgeAsset;
import model.element.commonKADS.Task;
import view.element.Association;
import view.ui.Logs;

public class BasicProperties extends WorkSheet{
    
    public BasicProperties(Element e, Logs log) {
        super(e, "Basic Properties", log);
        add_property_row(table, "name" ,e.getName());
        addValueChangeListener();
    } 
    public BasicProperties(Association a, Logs log) {
        super(a, "Basic Properties", log);
        add_property_row(table, "name" ,a.getName());
        addValueChangeListener();
    } 
    public void addValueChangeListener(){
        table.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("tableCellEditor".equals(evt.getPropertyName())) {
                    if (table.isEditing()){
                    }else{
                        updateName(evt);
                    }
                }
            }
        });
    }
   
    private void updateName(PropertyChangeEvent evt){
        TableEachRowRendererEditor t = (TableEachRowRendererEditor)evt.getSource();
        String editingProperty = ((AbstractModelTable)t.getModel()).getValueAt(table.getEditingRow(), 0).toString();

        if(editingProperty.equals("name")){
            if (   element instanceof Agent 
                || element instanceof Task
                || element instanceof KnowledgeAsset)
                   element.setName((String) table.getModel().getValueAt(table.getEditingRow(), table.getEditingColumn())); 
            else if(association != null){
                   association.setName((String) table.getModel().getValueAt(table.getEditingRow(), table.getEditingColumn())); 
            }
            else{
                log.printInfo("Name changed but you will not see it on UI Canvas. See BasicProperties class for more details");
            }
        }
    }
    
    public void tableUpdateNameElement(){
        if(element != null){
            table.getModel().setValueAt(element.getName(), 0, 1);
        } else if (association != null){
            table.getModel().setValueAt(association.getName(), 0, 1);
        }
    }
}
