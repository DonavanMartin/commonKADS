package view.workSheets;

import view.table.CellEditor;
import view.table.TableEachRowRendererEditor;
import view.table.TableCellListener;
import static view.table.jTableTools.add_property_row;
import view.ui.Properties;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import view.element.Association;
import view.element.Element;
import view.ui.Logs;

public abstract class WorkSheet extends JPanel{
    protected Element element;
    protected Association association;
    protected TableEachRowRendererEditor table;
    protected String name;
    
    private String model;
    private String workSheetName;
    private JLabel title;
    private JPanel titlePanel;
    private GridBagConstraints gridBagConstraints;
    private JPanel tablePanel;
    private JScrollPane tableScrollPanel;
    private ArrayList<Properties> rows;
    protected TableCellListener tcl;
    protected Logs log;
    
    public WorkSheet(Element e,String title, Logs log){
        this.element = e;
        association = null;
        init(title, log);
    }
    public WorkSheet(Association a,String title, Logs log){
        association = a;
        this.element = null;
        init(title, log);
    }
    private void init(String title, Logs log){
        this.log= log;
        this.title         = new JLabel();
        titlePanel         = new JPanel();
        tablePanel         = new JPanel();
        gridBagConstraints = new GridBagConstraints();
        tableScrollPanel   = new JScrollPane();
        table              = new TableEachRowRendererEditor(
                new Object [][] {},
                new String [] {"Property", "Value"}
        );
        
        table.getColumnModel().getColumn(1).setCellEditor(new CellEditor());

        
        workSheetName = title;
        this.title.setText(title);
        titlePanel.add(this.title, new java.awt.GridBagConstraints());
        this.title.getAccessibleContext().setAccessibleName(title);
        
        setLayout();
        setTitle();
        initTable();
        
        model =  "";
        rows = new ArrayList<Properties>();
    }

    
    
    public TableEachRowRendererEditor getTable(){
        return table;
    }
    

    
    
    private void setLayout(){
        setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setLayout(new java.awt.GridBagLayout());
    }
    
    private void setTitle(){
        titlePanel.setLayout(new java.awt.GridBagLayout());
        
        title.setText(workSheetName);
        titlePanel.add(title, new java.awt.GridBagConstraints());
        title.getAccessibleContext().setAccessibleName("title");
        
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth  = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.gridheight = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.fill   = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        add(titlePanel, gridBagConstraints);
    }
    
    private void initTable(){
        tablePanel.setLayout(new javax.swing.BoxLayout(tablePanel, javax.swing.BoxLayout.LINE_AXIS));
        tableScrollPanel = new JScrollPane();

        
        tcl = new TableCellListener(table);
        tableScrollPanel.setViewportView(table);
        table.getAccessibleContext().setAccessibleName("Properties_jTable");
        
        tablePanel.add(tableScrollPanel);
        
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.gridheight = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weighty = 1.0;
        add(tablePanel, gridBagConstraints);
    }
    
    public TableCellListener getTcl(){
        return tcl;
    }
    
    public String getModel() {
        return model;
    }
    
    public void setModel(String model) {
        this.model = model;
    }
    
    public String getWorkSheetName() {
        return workSheetName;
    }
    
    public void setWorkSheetName(String name) {
        this.workSheetName = name;
    }
    
    public ArrayList<Properties> getProperties() {
        return rows;
    }
    
    public void setProperties(ArrayList<Properties> properties) {
        this.rows = properties;
    }
    
    public void addProperty(Properties p){
        rows.add(p);
    }
    
    public void remProperties(Properties p){
        rows.remove(p);
    }
    
}
