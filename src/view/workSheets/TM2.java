package view.workSheets;

import view.table.AbstractModelTable;
import view.table.TableEachRowRendererEditor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import view.element.Element;
import view.ui.Logs;

public class TM2 extends WorkSheet{
    private String possessed_by;
    private String used_in;
    private String domain;
    
    /* Array of boolean 
        boolean[0] = Is it ?
        boolean[1] = Bottleneck / to be improved ?
    */
    
    // Nature of the knowledge
    private boolean formal_rigourus[];
    private boolean empirical_quantitative[];
    private boolean heuristic_rules_of_thumbs[];
    private boolean highly_specialized_domain_specific[];
    private boolean experience_based[];
    private boolean action_based[];
    private boolean incomplete[];
    private boolean uncertain_may_be_incorrect[];
    private boolean quickly_changing[];
    private boolean hard_to_verify[];
    private boolean tacit_hard_to_transfert[];
    
    // Form of the knowledge
    private boolean mind[];
    private boolean paper[];
    private boolean electronic[];
    private boolean action_skill[];
    private boolean other[];
    
    // Availability of knowledge
    private boolean limitation_in_time[];
    private boolean limitation_in_spce[];
    private boolean limitation_in_access[];
    private boolean limitation_in_quality[];
    private boolean limitation_in_form[];

    public TM2(Element element, Logs log) {
        super(element, "Specification of the knowledge employed for a task, and possible bottlenecks and areas for improvement. TM-2", log);
         this.element = element;
        name = "";
        
       
        // Nature of the knowledge
        empirical_quantitative              = new boolean[2];
        heuristic_rules_of_thumbs           = new boolean[2];
        highly_specialized_domain_specific  = new boolean[2];
        experience_based                    = new boolean[2];
        action_based                        = new boolean[2];
        incomplete                          = new boolean[2];
        uncertain_may_be_incorrect          = new boolean[2];
        quickly_changing                    = new boolean[2];
        hard_to_verify                      = new boolean[2];
        tacit_hard_to_transfert             = new boolean[2];
        
        // Form of the knowledge
        mind                                = new boolean[2];
        paper                               = new boolean[2];
        electronic                          = new boolean[2];
        action_skill                        = new boolean[2];
        other                               = new boolean[2];

        // Availability of knowledge
        limitation_in_time                  = new boolean[2];
        limitation_in_spce                  = new boolean[2];
        limitation_in_access                = new boolean[2];
        limitation_in_quality               = new boolean[2];   
        limitation_in_form                  = new boolean[2];  
        //addValueChangeListener();
    }
    /*
    public void addValueChangeListener(){
        table.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("tableCellEditor".equals(evt.getPropertyName())) {
                    if (table.isEditing()){
                        processEditingStarted();
                    }else{
                        processEditingStopped(evt);
                    }
                }
            }
            private void processEditingStopped(PropertyChangeEvent evt) {
                TableEachRowRendererEditor t = (TableEachRowRendererEditor)evt.getSource();
                String editingProperty = ((AbstractModelTable)t.getModel()).getValueAt(table.getEditingRow(), 0).toString();
                switch (editingProperty){
                    case "no"                        : 
                    case "name"                      : element.setName((String) table.getModel().getValueAt(table.getEditingRow(), table.getEditingColumn())); 
                    //case "goal and value"           : 
                    //case "dependency tasks"         : 
                    //case "flow tasks"               : 
                    //case "inputs objects"           : 
                    //case "outputs objects"          : 
                    //case "timing and control"       : 
                    //case "agents"                   : 
                    //case "knowledge and competences": 
                    //case "ressources"               : 
                    //case "quality and performance"  : 
                    default : log.printError("Property Chaged:" +  evt.getPropertyName() + "Not Set Yet for TM1");
                
                }
            }
            
            private void processEditingStarted() {
                 
            }
        });
    }*/
    
}
