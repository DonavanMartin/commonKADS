package view.workSheets;

import view.table.AbstractModelTable;
import view.table.TableEachRowRendererEditor;
import static view.table.jTableTools.add_property_row;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import view.element.Element;
import model.element.commonKADS.KnowledgeAsset;
import view.ui.Logs;

public class OM3 extends WorkSheet{
    private int no;
    
    private String performedBy;
    private String where;
    private boolean isIntensive;
    private boolean signifiance;
    private ArrayList<KnowledgeAsset> inputs;
    private ArrayList<KnowledgeAsset> outputs;
    
    public OM3(Element element, Logs log) {
        super(element, "Process Breakdown Worksheet OM-3", log);
        this.element = element;
        no = 0;
        name = "";
        performedBy = "";
        where = "";
        isIntensive =false;
        signifiance = false;
        inputs = new ArrayList<>();
        outputs = new ArrayList<>();
        
        initRows();
        addValueChangeListener();
    }
    
    public void initRows(){
        add_property_row(table, "no"            ,no);
        add_property_row(table, "name"          ,name);
        add_property_row(table, "performedBy"   ,performedBy);
        add_property_row(table, "where"         ,where);
        add_property_row(table, "isIntensive"   ,isIntensive);
        add_property_row(table, "signifiance"   ,signifiance);
        add_property_row(table, "inputs"        ,inputs);
        add_property_row(table, "outputs"       ,outputs);
    }
        public void addValueChangeListener(){
        table.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("tableCellEditor".equals(evt.getPropertyName())) {
                    if (table.isEditing()){
                        processEditingStarted();
                    }else{
                        processEditingStopped(evt);
                    }
                }
            }
            private void processEditingStopped(PropertyChangeEvent evt) {
                TableEachRowRendererEditor t = (TableEachRowRendererEditor)evt.getSource();
                String editingProperty = ((AbstractModelTable)t.getModel()).getValueAt(table.getEditingRow(), 0).toString();
                switch (editingProperty){
                    case "no"                        : 
                    case "name"                      : element.setName((String) table.getModel().getValueAt(table.getEditingRow(), table.getEditingColumn())); 
                    //case "goal and value"           : 
                    //case "dependency tasks"         : 
                    //case "flow tasks"               : 
                    //case "inputs objects"           : 
                    //case "outputs objects"          : 
                    //case "timing and control"       : 
                    //case "agents"                   : 
                    //case "knowledge and competences": 
                    //case "ressources"               : 
                    //case "quality and performance"  : 
                    default : log.printError("Property Chaged:" +  evt.getPropertyName() + "Not Set Yet for TM1");
                
                }
            }
            
            private void processEditingStarted() {
                 
            }
        });
    }
    
    public void addInputs(KnowledgeAsset ka){
        inputs.add(ka);
    }
    
    public void addOutputs(KnowledgeAsset ka){
        outputs.add(ka);
    }
    
    public int getNo() {
        return no;
    }
    
    public void setNo(int no) {
        this.no = no;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getPerformedBy() {
        return performedBy;
    }
    
    public void setPerformedBy(String performedBy) {
        this.performedBy = performedBy;
    }
    
    public String getWhere() {
        return where;
    }
    
    public void setWhere(String where) {
        this.where = where;
    }
    
    public boolean isIsIntensive() {
        return isIntensive;
    }
    
    public void setIsIntensive(boolean isIntensive) {
        this.isIntensive = isIntensive;
    }
    
    public boolean isSignifiance() {
        return signifiance;
    }
    
    public void setSignifiance(boolean signifiance) {
        this.signifiance = signifiance;
    }
    
    public ArrayList<KnowledgeAsset> getInputs() {
        return inputs;
    }
    
    public void setInputs(ArrayList<KnowledgeAsset> inputs) {
        this.inputs = inputs;
    }
    
    public ArrayList<KnowledgeAsset> getOutputs() {
        return outputs;
    }
    
    public void setOutputs(ArrayList<KnowledgeAsset> outputs) {
        this.outputs = outputs;
    }
    
    // Add
    public void addInputsKnowledgeAsset(KnowledgeAsset k){
        inputs.add(k);
    }
    public void addOutputsKnowledgeAsset(KnowledgeAsset k){
        outputs.add(k);
    }
    
    // REmove
    public void removeInputsKnowledgeAsset(KnowledgeAsset k){
        inputs.remove(k);
    }
    public void removeOutputsKnowledgeAsset(KnowledgeAsset k){
        outputs.remove(k);
    }
}
