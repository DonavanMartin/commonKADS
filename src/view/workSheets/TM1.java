package view.workSheets;

import view.table.AbstractModelTable;
import view.table.TableEachRowRendererEditor;
import static view.table.jTableTools.add_property_row;
import static view.table.jTableTools.updateRowHeights;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import view.element.Element;
import model.element.commonKADS.Agent;
import model.element.commonKADS.KnowledgeAsset;
import model.element.commonKADS.Task;
import view.ui.Logs;

public class TM1 extends WorkSheet{
    private int no;
    
    private String organization;
    private String goal_and_value;
    private String knowledge_and_competence;
    private String ressources;
    private String quality_and_performance;
    private String timing_and_control;
    
    // Dependency and flow
    private ArrayList<Task> inputsTasks;
    private ArrayList<Task> outputsTasks;
    
    // Object Handled
    private ArrayList<KnowledgeAsset> inputsKnowledges;
    private ArrayList<KnowledgeAsset> outputsKnowledges;
    
    private ArrayList<Agent> agents;
    
    public TM1(Element e, Logs log) {
        super(e, "First analysis of the application-assessment task TM-1", log);
        element = e;
        no = 0;
        name = "";
        organization = "";
        goal_and_value = "";
        knowledge_and_competence = "";
        ressources = "";
        quality_and_performance = "";
        timing_and_control = "";
        
        inputsKnowledges  = new ArrayList<>();
        outputsKnowledges = new ArrayList<>();
        inputsTasks       = new ArrayList<>();
        outputsTasks      = new ArrayList<>();
        agents            = new ArrayList<>();
        initRows();
        addValueChangeListener();
    }
    
    public void initRows(){
        add_property_row(table, "no"                        ,no);
        add_property_row(table, "name"                      ,name);
        add_property_row(table, "goal and value"            ,goal_and_value);
        add_property_row(table, "dependency tasks"          ,inputsTasks);
        add_property_row(table, "flow tasks"                ,outputsTasks);
        add_property_row(table, "inputs objects"            ,inputsKnowledges);
        add_property_row(table, "outputs objects"           ,outputsKnowledges);
        add_property_row(table, "timing and control"        ,timing_and_control);
        add_property_row(table, "agents"                    ,agents);
        add_property_row(table, "knowledge and competences" ,knowledge_and_competence);
        add_property_row(table, "ressources"                ,ressources);
        add_property_row(table, "quality and performance"   ,quality_and_performance);
    }
    
    public void addValueChangeListener(){
        table.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("tableCellEditor".equals(evt.getPropertyName())) {
                    if (table.isEditing()){
                        processEditingStarted();
                    }else{
                        processEditingStopped(evt);
                    }
                }
            }
            private void processEditingStopped(PropertyChangeEvent evt) {
                TableEachRowRendererEditor t = (TableEachRowRendererEditor)evt.getSource();
                String editingProperty = ((AbstractModelTable)t.getModel()).getValueAt(table.getEditingRow(), 0).toString();
                switch (editingProperty){
                    case "no"                        : 
                    case "name"                      : element.setName((String) table.getModel().getValueAt(table.getEditingRow(), table.getEditingColumn())); 
                    //case "goal and value"           : 
                    //case "dependency tasks"         : 
                    //case "flow tasks"               : 
                    //case "inputs objects"           : 
                    //case "outputs objects"          : 
                    //case "timing and control"       : 
                    //case "agents"                   : 
                    //case "knowledge and competences": 
                    //case "ressources"               : 
                    //case "quality and performance"  : 
                    default : log.printError("Property Chaged:" +  evt.getPropertyName() + "Not Set Yet for TM1");
                
                }
            }
            
            private void processEditingStarted() {
                 
            }
        });
    }
    
    public int getNo() {
        return no;
    }
    
    public void setNo(int no) {
        this.no = no;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getOrganization() {
        return organization;
    }
    
    public void setOrganization(String organization) {
        this.organization = organization;
    }
    
    public String getGoal_and_value() {
        return goal_and_value;
    }
    
    public void setGoal_and_value(String goal_and_value) {
        this.goal_and_value = goal_and_value;
    }
    
    public String getKnowledge_and_competence() {
        return knowledge_and_competence;
    }
    
    public void setKnowledge_and_competence(String knowledge_and_competence) {
        this.knowledge_and_competence = knowledge_and_competence;
    }
    
    public String getRessources() {
        return ressources;
    }
    
    public void setRessources(String ressources) {
        this.ressources = ressources;
    }
    
    public String getQuality_and_performance() {
        return quality_and_performance;
    }
    
    public void setQuality_and_performance(String quality_and_performance) {
        this.quality_and_performance = quality_and_performance;
    }
    
    public String getTiming_and_control() {
        return timing_and_control;
    }
    
    public void setTiming_and_control(String timing_and_control) {
        this.timing_and_control = timing_and_control;
    }
    
    public ArrayList<Task> getInputsTasks() {
        return inputsTasks;
    }
    
    public void setInputsTasks(ArrayList<Task> inputsTasks) {
        this.inputsTasks = inputsTasks;
    }
    
    public ArrayList<Task> getOutputsTasks() {
        return outputsTasks;
    }
    
    public void setOutputsTasks(ArrayList<Task> outputsTasks) {
        this.outputsTasks = outputsTasks;
    }
    
    public ArrayList<KnowledgeAsset> getInputsKnowledges() {
        return inputsKnowledges;
    }
    
    public void setInputsKnowledges(ArrayList<KnowledgeAsset> inputsKnowledges) {
        this.inputsKnowledges = inputsKnowledges;
    }
    
    public ArrayList<KnowledgeAsset> getOutputsKnowledges() {
        return outputsKnowledges;
    }
    
    public void setOutputsKnowledges(ArrayList<KnowledgeAsset> outputsKnowledges) {
        this.outputsKnowledges = outputsKnowledges;
    }
    
    public ArrayList<Agent> getAgents() {
        return agents;
    }
    
    public void setAgents(ArrayList<Agent> agents) {
        this.agents = agents;
    }
    
    
    // Add
    public void addInputsTasks(Task t){
        inputsTasks.add(t);
    };
    
    public void addOutputsTasks(Task t){
        outputsTasks.add(t);
    };
    
    public void addInputsKnowledgeAsset(KnowledgeAsset k){
        inputsKnowledges.add(k);
    };

    public void addOutputsKnowledgeAsset(KnowledgeAsset k){
        outputsKnowledges.add(k);
    };
    
    // Remove
    public void removeInputsTasks(Task t){
        inputsTasks.remove(t);
    };
    
    public void removeOutputsTasks(Task t){
        outputsTasks.remove(t);
    };
    
    public void removeInputsKnowledgeAsset(KnowledgeAsset k){
        inputsKnowledges.remove(k);
    };

    public void removeOutputsKnowledgeAsset(KnowledgeAsset k){
        outputsKnowledges.remove(k);
    };

}
