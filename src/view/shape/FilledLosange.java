package view.shape;

import view.element.Association;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import model.element.commonKADS.Decision;
import view.element.Element;

public class FilledLosange extends ElementShape{
    private Diamond diamond;
    
    
    public FilledLosange(){
        super();
        shapeType = "filledLosange";
        contentShape = new Ellipse2D.Double();
        highlightShape = new Ellipse2D.Double();
        diamond = new Diamond(20, 20);
    }
    
    public void draw(Graphics2D g2, Element e) {
        //Operator operator = (Operator)e;
        
        if(e != null) {
            
            Graphics2D g2d = (Graphics2D) g2.create();
            int x = (getWidth() - diamond.getBounds().width) / 2;
            int y = (100- diamond.getBounds().height) / 2;
            AffineTransform at = AffineTransform.getTranslateInstance(contentShape.getBounds().getLocation().getX(), contentShape.getBounds().getLocation().getY());
            Shape shape = at.createTransformedShape(diamond);
            
            Color color = Color.BLACK;
            
            if(e.isWarned()){
                color = Color.RED;
            }else if(e instanceof Decision){
                if(((Decision) e).isInOutWarning()){
                    color = Color.RED;
                }
            }
            g2d.setColor(color);
            g2d.fill(shape);
            g2d.setColor(color);
            g2d.draw(shape);
            g2d.dispose();
            if(e instanceof Decision){
                FontMetrics fm = g2.getFontMetrics(e.getFont());
                int width = fm.stringWidth(((Decision)e).getDecision().getCondition());
                g2.drawString(((Decision)e).getDecision().getCondition(),
                        (int)((Ellipse2D)contentShape).getCenterX()-width/2+1,
                        (int)((Ellipse2D)contentShape).getCenterY()-12);
            }
            for(Association a : e.getFromAssociation()){
                a.draw(g2);
            }
            
            for(Association a : e.getPointerAssociation()){
                a.move(e);
                a.draw(g2);
            }
            if(e.getListAssociation().size() > 0){
                for(Association association : e.getListAssociation()) {
                    association.move(e);
                    association.draw(g2);
                }
            }
            
        }
    }
    public class Diamond extends Path2D.Double {
        
        public Diamond(double width, double height) {
            moveTo(0, height / 2);
            lineTo(width / 2, 0);
            lineTo(width, height / 2);
            lineTo(width / 2, height);
            closePath();
        }
        
    }
    
}
