package view.shape;


import view.design.ColorPalette;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import view.element.Corner;
import view.element.Element;

public abstract class ElementShape {
    protected Point location; // Upper left corner
   
    protected int lastX;
    protected int lastY;
    
    protected int leftDragLimit;
    protected int rightDragLimit;
    protected int upperDragLimit;
    protected int lowerDragLimit;
    
    protected int nameWidth;
    protected int width;
    public int contentHeight;
    protected String shapeType;  
    
    protected Corner upperLeftHandle;
    protected Corner upperRightHandle;
    protected Corner lowerLeftHandle;
    protected Corner lowerRightHandle;
    protected Corner selectedHandle;
    
    private boolean isResizing;

    protected Shape nameShape;
    protected Shape contentShape;
    protected Shape highlightShape;

    public int NAME_HEIGHT = 18;
    protected final int LIMIT_HEIGHT = 18;
    public final int HIGHLIGHT_THICKNESS = 1;
    public final int HIGHLIGHT_PADDING = 1;
    public final int PADDING = 5;
    public final double RADIUS = 20;
    public final Color PROBLEM_COLOR  = Color.RED;
    public final Color SELECTED_COLOR = Color.BLUE;
    public final Color highlightColor = ColorPalette.BLUELIGHT;
    
    public static final int UPPERLEFT  = 0;
    public static final int UPPERRIGHT = 1;
    public static final int LOWERLEFT  = 2;
    public static final int LOWERRIGHT = 3;

    public abstract void draw(Graphics2D g2, Element e);
    
    public ElementShape(){
        nameShape = null;
        contentShape = null;
        highlightShape = null;
        shapeType = ""; 
        isResizing = false;
        
        location = null;

        upperLeftHandle  = new Corner();
        upperRightHandle = new Corner();
        lowerLeftHandle  = new Corner();
        lowerRightHandle = new Corner();
        selectedHandle = null;
    }    
    public void setOldPosition(int x , int y){
        lastX = x;
        lastY = y;
    }

    public Corner getUpperLeftHandle() {
        return upperLeftHandle;
    }

    public Corner getUpperRightHandle() {
        return upperRightHandle;
    }

    public Corner getLowerLeftHandle() {
        return lowerLeftHandle;
    }

    public Corner getLowerRightHandle() {
        return lowerRightHandle;
    }

    public Corner getSelectedHandle() {
        return selectedHandle;
    }

    public void setUpperLeftHandle(Corner upperLeftHandle) {
        this.upperLeftHandle = upperLeftHandle;
    }

    public void setUpperRightHandle(Corner upperRightHandle) {
        this.upperRightHandle = upperRightHandle;
    }

    public void setLowerLeftHandle(Corner lowerLeftHandle) {
        this.lowerLeftHandle = lowerLeftHandle;
    }

    public void setLowerRightHandle(Corner lowerRightHandle) {
        this.lowerRightHandle = lowerRightHandle;
    }

    public void setSelectedHandle(Corner selectedHandle) {
        this.selectedHandle = selectedHandle;
    }
    
    public void setLocation(Point location) {
        this.location = location;
    }

    public boolean isIsResizing() {
        return isResizing;
    }
    
    public int getNameWidth() {
        return nameWidth;
    }

    public void setNameWidth(int nameWidth) {
        this.nameWidth = nameWidth;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getContentHeight() {
        return contentHeight;
    }

    public void setContentHeight(int contentHeight) {
        this.contentHeight = contentHeight;
    }

    public Shape getNameShape() {
        return nameShape;
    }

    public void setNameShape(Shape nameShape) {
        this.nameShape = nameShape;
    }

    public Shape getContentShape() {
        return contentShape;
    }

    public void setContentShape(Shape contentShape) {
        this.contentShape = contentShape;
    }

    public Shape getHighlightShape() {
        return highlightShape;
    }

    public void setHighlightShape(Shape highlightShape) {
        this.highlightShape = highlightShape;
    }

    public String getShapeType() {
        return shapeType;
    }

    public void setShapeType(String shape) {
        this.shapeType = shape;
    }
    
    public Point getLocation() {
        return location;
    }

    public Point getCenterPoint() {
        return new Point((int)(getLocation().getX()+RADIUS), (int)(getLocation().getY()+RADIUS));
    }

    
    public boolean isInSelectionZone(Point point) {
        switch(shapeType){
            case "titleledRectangle":       return isTitleledRectangleInSelectionZone(point);
            case "simpleRectangle":         return isSimpleRectanglInSelectionZone(point);
            case "labeledCircle":           return isCircleInSelectionZone(point);
            case "titleledRoundRectangle":  return isTitleledRoundRectangleInSelectionZone(point);
            case "agentShape":              return isAgentShapeInSelectionZone(point);
            case "simpleRoundRectangle":    return isSimpleRoundRectangle(point);
            case "filledCircle":            return isCircleInSelectionZone(point);
            case "filledLosange":           return isLosangeInSelectionZone(point);
            case "multiCircle":             return isCircleInSelectionZone(point);

            default: System.err.println("IsInSelectionZone not implemented yet for:"+ shapeType );
        }
        return false;
    }
    private boolean isSimpleRoundRectangle(Point point){
        return contentShape.contains(point);
    }
    private boolean isTitleledRectangleInSelectionZone(Point point){
        return nameShape.contains(point);
    }
    private boolean isSimpleRectanglInSelectionZone(Point point){
        return contentShape.contains(point);
    } 
    
    private boolean isLosangeInSelectionZone(Point point){
        return contentShape.contains(point);
    }    
    private boolean isCircleInSelectionZone(Point point){
        return contentShape.contains(point);
    }
    
    private boolean isTitleledRoundRectangleInSelectionZone(Point point){
        return nameShape.contains(point);
    }
    private boolean isAgentShapeInSelectionZone(Point point){
        return nameShape.contains(point);
    }
    
    public Rectangle getBounds() {
        switch(shapeType){
            case "titleledRectangle":       return getTitleledRectangleBounds();
            case "simpleRectangle":         return getSimpleRectangleBounds();
            case "labeledCircle":           return getCircle();
            case "titleledRoundRectangle":  return getTitleledRoundRectangleBounds();
            case "agentShape":              return getAgentShapeBounds();
            case "simpleRoundRectangle":    return getSimpleRoundRectangle();
            case "filledCircle":            return getCircle();
            case "multiCircle":             return getCircle();
            case "filledLosange":           return getCircle();

            default: System.err.println("Get Bounds not implemented yet for:"+ shapeType );
        }
        return null;
    }
    private Rectangle getSimpleRoundRectangle(){
        return ((Rectangle)contentShape.getBounds());
    }
    
    private Rectangle getTitleledRoundRectangleBounds(){
        return ((Rectangle)nameShape.getBounds()).union(((Rectangle)contentShape.getBounds()));
    }
        
    private Rectangle getAgentShapeBounds(){
        return ((Rectangle)nameShape.getBounds()).union((((Polygon)contentShape).getBounds()));
    }
    
    private Rectangle getCircle(){
        Rectangle bounds = new Rectangle((int)RADIUS, (int)RADIUS);
        bounds.setLocation(getLocation());
        return bounds;
    }
    
    private Rectangle getTitleledRectangleBounds(){
        return ((Rectangle)nameShape).union(((Rectangle)contentShape));
    }
   
    private Rectangle getSimpleRectangleBounds(){
        return (Rectangle)contentShape;
    }
    
    public void setBounds(int x, int y) {
        switch(shapeType){
            case "titleledRectangle":       setTitleledRectangleBounds(x,y); break;
            case "simpleRectangle":         setSimpleRectangleBounds  (x,y); break;
            case "titleledRoundRectangle":  setTitleledRoundRectangleBounds (x,y); break;
            case "agentShape":              setAgentShapeBounds(x,y); break;
            case "simpleRoundRectangle":    setSimpleRoundRectangle(x,y); break;
            case "filledCircle":            setCircleBounds(x,y); break;
            case "filledLosange":           setCircleBounds(x,y); break;
            case "multiCircle":             setCircleBounds(x,y); break;

            default: System.err.println("Set Bounds not implemented yet for:"+ shapeType );
        }
    }
    
    private void setCircleBounds(int x , int y){
        ((Ellipse2D)contentShape).setFrame(x, y, RADIUS, RADIUS);
        int offset = HIGHLIGHT_THICKNESS + HIGHLIGHT_PADDING;
        ((Ellipse2D)highlightShape).setFrame(x-offset, y-offset, RADIUS+offset*2, RADIUS+offset*2);
    }
    
    private void setSimpleRectangleBounds(int x , int y){
        ((Rectangle)contentShape).setBounds(x, y+NAME_HEIGHT, width, contentHeight);
        int offset = HIGHLIGHT_THICKNESS + HIGHLIGHT_PADDING;
        ((Rectangle)highlightShape).setBounds(x-offset, y-offset, width+offset*2, NAME_HEIGHT+contentHeight); 
    }
    
    private void setSimpleRoundRectangle(int x , int y){
        Double arcWidth  = TitleledRoundRectangle2D.arcWidth;
        Double arcHeight = TitleledRoundRectangle2D.arcHeigth;
        ((RoundRectangle2D)contentShape).setRoundRect(x, y+NAME_HEIGHT, width, contentHeight,arcWidth,arcHeight);
        int offset = HIGHLIGHT_THICKNESS + HIGHLIGHT_PADDING;
        ((RoundRectangle2D)highlightShape).setRoundRect(x-offset, y-offset, width+offset*2, NAME_HEIGHT+contentHeight,arcWidth,arcHeight); 
    
    }
    
    private void setTitleledRectangleBounds(int x , int y){
        ((Rectangle)nameShape).setBounds(x, y, width, NAME_HEIGHT);
        ((Rectangle)contentShape).setBounds(x, y+NAME_HEIGHT, width, contentHeight);
        int offset = HIGHLIGHT_THICKNESS + HIGHLIGHT_PADDING;
        ((Rectangle)highlightShape).setBounds(x-offset, y-offset, width+offset*2, NAME_HEIGHT+contentHeight+offset*2); 
    }
   
    private void setTitleledRoundRectangleBounds(int x , int y){
        Double arcWidth  = TitleledRoundRectangle2D.arcWidth;
        Double arcHeight = TitleledRoundRectangle2D.arcHeigth;
        ((RoundRectangle2D)nameShape).setRoundRect(x, y, width, NAME_HEIGHT,arcWidth,arcHeight);
        ((RoundRectangle2D)contentShape).setRoundRect(x, y+NAME_HEIGHT, width, contentHeight,arcWidth,arcHeight);
        int offset = HIGHLIGHT_THICKNESS + HIGHLIGHT_PADDING;
        ((RoundRectangle2D)highlightShape).setRoundRect(x-offset, y-offset, width+offset*2, NAME_HEIGHT+offset*2,arcWidth,arcHeight); 
    }

    private void setAgentShapeBounds(int x , int y){
        ((Rectangle)nameShape).setBounds(x, y, width, NAME_HEIGHT);
        //((Polygon)contentShape).setBounds(x, y+NAME_HEIGHT, width, contentHeight);
        int offset = HIGHLIGHT_THICKNESS + HIGHLIGHT_PADDING;
        ((Rectangle)highlightShape).setBounds(x-offset, y-offset, width+offset*2, NAME_HEIGHT+contentHeight+offset*2); 
        
        
        // Translate to good position
        ((Polygon)contentShape).translate(-lastX, -lastY);
        ((Polygon)contentShape).translate(x, y);
        setOldPosition(x, y);
    }    
    
    // Used in : ResizeController.java
    public void setIsResizing(boolean r) {
        isResizing = r;
        
        setCornerHandleLocation();
    }

    public void setFirstAgentHandleLocation(){
         Point point = getLocation();
        upperLeftHandle.setLocation(new Point   (0,         0));
        upperRightHandle.setLocation(new Point  (width,     0));
        lowerLeftHandle.setLocation(new Point   (0,         (int)point.getY()+NAME_HEIGHT+contentHeight));
        lowerRightHandle.setLocation(new Point  (width,     (int)point.getY()+NAME_HEIGHT+contentHeight));
    }
    
    public void setCornerHandleLocation(){
        Point point = getLocation();
        upperLeftHandle.setLocation(point);
        upperRightHandle.setLocation(new Point((int)point.getX()+width, (int)point.getY()));
        lowerLeftHandle.setLocation(new Point( (int)point.getX(), (int)point.getY()+NAME_HEIGHT+contentHeight));
        lowerRightHandle.setLocation(new Point((int)point.getX()+width, (int)point.getY()+NAME_HEIGHT+contentHeight));
    }
    
    public void resizeLeft(int newX){
            upperLeftHandle.setLocation(new Point(newX, (int)upperLeftHandle.getLocation().getY()));
            lowerLeftHandle.setLocation(new Point(newX, (int)lowerLeftHandle.getLocation().getY()));
            int oldX = (int)getLocation().getX();
            width -= newX - oldX;
    }
    
    public void resizeRight(int newX){
            upperRightHandle.setLocation(new Point(newX, (int)upperRightHandle.getLocation().getY()));
            lowerRightHandle.setLocation(new Point(newX, (int)lowerRightHandle.getLocation().getY()));
            int oldX = (int)getLocation().getX() + width;
            width -= oldX - newX;
    }
    
    public void resizeUpperY(int newY){
        upperLeftHandle.setLocation(new Point((int)upperLeftHandle.getLocation().getX(), newY));
        upperRightHandle.setLocation(new Point((int)upperRightHandle.getLocation().getX(), newY));
        int oldY = (int)getLocation().getY();
        contentHeight -= newY - oldY;
    }
   public void resizeLowerY(int newY){
        lowerRightHandle.setLocation(new Point((int)lowerRightHandle.getLocation().getX(), newY));
            lowerLeftHandle.setLocation(new Point((int)lowerLeftHandle.getLocation().getX(), newY));
            int oldY = (int)getLocation().getY() + NAME_HEIGHT + contentHeight;
            contentHeight -= oldY - newY;
    }
        
    public boolean resizeUpperLeft(int newX, int newY){
        boolean changed = false;
        if((newX > leftDragLimit) && (newX < rightDragLimit)) {
            resizeLeft(newX);
            changed = true;
        }
        // resize height
        if((newY > upperDragLimit) && (newY < lowerDragLimit)) {
            resizeUpperY(newY);
            changed = true;
        }
        return changed;
    }
    
    public boolean resizeUpperRight(int newX, int newY){
        boolean changed = false;
        // resize width
        if(newX > leftDragLimit) {
            resizeRight(newX);
            changed = true;
        }
        // resize height
        if((newY > upperDragLimit) && (newY < lowerDragLimit)) {
            resizeUpperY(newY);
            changed = true;
        }
        return changed;
    }
    
    public boolean resizeLowerLeft(int newX, int newY){
        boolean changed = false;
        // resize width
        if((newX > leftDragLimit) && (newX < rightDragLimit)) {
            resizeLeft(newX);
            changed = true;
        }
        // resize height
        if(newY > upperDragLimit) {
            resizeLowerY(newY);
            changed = true;
        }
        return changed;
    }
    
    public boolean resizeLowerRight(int newX, int newY){
        boolean changed = false;
        // resize width
        if(newX > leftDragLimit) {
           resizeRight(newX);
            changed = true;
        }
        // resize height
        if(newY > upperDragLimit) {
            resizeLowerY(newY);
            changed = true;
        }
        return changed;
    }
    
    public int getHandleType(){
        if(selectedHandle == upperLeftHandle)
            return UPPERLEFT;
        else if(selectedHandle == upperRightHandle)
            return UPPERRIGHT;
        else if(selectedHandle == lowerLeftHandle)
            return LOWERLEFT;
        else if(selectedHandle == lowerRightHandle)
            return LOWERRIGHT;
        else
            System.err.println("Error type Handle!");
        return -1;
    }
    
    
    public int setHandleType(int cornerSelected){     
            if ( cornerSelected == UPPERLEFT  ) { return 1;}
       else if ( cornerSelected == UPPERRIGHT ) { return 2;}
       else if ( cornerSelected == LOWERLEFT  ) { return 3;}
       else if ( cornerSelected == LOWERRIGHT ) { return 4;}
       else{ return -1; }    
    }
    
    public int getHandleType(Point point){
        if(upperLeftHandle.contains(point))
            return UPPERLEFT;
        else if(upperRightHandle.contains(point))
            return UPPERRIGHT;
        else if(lowerLeftHandle.contains(point))
            return LOWERLEFT;
        else if(lowerRightHandle.contains(point))
            return LOWERRIGHT;
        return -1;
    }
    
   public Corner findHandle(Point point) {
        Corner handle = null;
        if(upperLeftHandle.contains(point))
            handle = upperLeftHandle;
        else if(upperRightHandle.contains(point))
            handle = upperRightHandle;
        else if(lowerLeftHandle.contains(point))
            handle = lowerLeftHandle;
        else if(lowerRightHandle.contains(point))
            handle = lowerRightHandle;
        return handle;
    }  
    
    
    public void setSelectedHandle(Point point) {
        selectedHandle = findHandle(point);
        computeDragLimits();
    }
    
    public void unsetSelectedHandle() {
        selectedHandle = null;
    }
    
    public void computeDragLimits() {
        leftDragLimit = 0;
        rightDragLimit = 0;
        upperDragLimit = 0;
        lowerDragLimit = 0;
        if(selectedHandle == upperLeftHandle) {
            rightDragLimit = (int)upperRightHandle.getLocation().getX() - nameWidth - PADDING;
            lowerDragLimit = (int)lowerLeftHandle.getLocation().getY() - LIMIT_HEIGHT;
        }
        else if(selectedHandle == upperRightHandle) {
            leftDragLimit = (int)upperLeftHandle.getLocation().getX() + nameWidth;
            lowerDragLimit = (int)lowerLeftHandle.getLocation().getY() - LIMIT_HEIGHT;
        }
        else if(selectedHandle == lowerLeftHandle) {
            rightDragLimit = (int)upperRightHandle.getLocation().getX() - nameWidth - PADDING;
            upperDragLimit = (int)upperLeftHandle.getLocation().getY() + LIMIT_HEIGHT;
        }
        else if(selectedHandle == lowerRightHandle) {
            leftDragLimit = (int)upperLeftHandle.getLocation().getX() + nameWidth;
            upperDragLimit = (int)upperLeftHandle.getLocation().getY() + LIMIT_HEIGHT;
        }
    }
    public void cumputAllDragLimit(){
        leftDragLimit = 0;
        rightDragLimit = 0;
        upperDragLimit = 0;
        lowerDragLimit = 0;
        rightDragLimit = (int)upperRightHandle.getLocation().getX() - nameWidth - PADDING;
        lowerDragLimit = (int)lowerLeftHandle.getLocation().getY()  - LIMIT_HEIGHT;
        leftDragLimit  = (int)upperLeftHandle.getLocation().getX()  + nameWidth;       
        upperDragLimit = (int)upperLeftHandle.getLocation().getY()  + LIMIT_HEIGHT;
    }
    
    public void resize(Point point) {   
        int newX = (int)point.getX();
        int newY = (int)point.getY();
        boolean changed = false;
        
        switch( getHandleType()){
            case UPPERLEFT : changed = resizeUpperLeft (newX, newY); break;
            case UPPERRIGHT: changed = resizeUpperRight(newX, newY); break;
            case LOWERLEFT : changed = resizeLowerLeft (newX, newY); break;
            case LOWERRIGHT: changed = resizeLowerRight(newX, newY); break;
            default: break;
        }
        
        if(changed) {
            setLocation(upperLeftHandle.getLocation());
            int x = (int)getLocation().getX();
            int y = (int)getLocation().getY();
            setBounds(x, y);
        }
    }    
    
    public void resize(Point point, int selectedCorner) {   
        
        int newX = (int)point.getX();
        int newY = (int)point.getY();
        boolean changed = false;
        
        switch(selectedCorner){
            case UPPERLEFT : changed = resizeUpperLeft (newX, newY); break;
            case UPPERRIGHT: changed = resizeUpperRight(newX, newY); break;
            case LOWERLEFT : changed = resizeLowerLeft (newX, newY); break;
            case LOWERRIGHT: changed = resizeLowerRight(newX, newY); break;
            default: break;
        }
        
        if(changed) {
            setLocation(upperLeftHandle.getLocation());
            int x = (int)getLocation().getX();
            int y = (int)getLocation().getY();
            setBounds(x, y);
        }
    }    
    
}
