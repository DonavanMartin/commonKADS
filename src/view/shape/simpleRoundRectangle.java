package view.shape;

import view.element.Associable;
import view.element.Association;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;
import view.element.Element;

public class simpleRoundRectangle extends ElementShape{
    
    public simpleRoundRectangle(){
        super();
        shapeType = "simpleRoundRectangle";
        contentShape   = new RoundRectangle2D.Float();
        highlightShape = new RoundRectangle2D.Float();
        
        width = 50;
        contentHeight = 20;
        nameWidth = 0;
        NAME_HEIGHT = 0;
    }
    
    @Override
    public void draw(Graphics2D g2, Element e){
        
        g2.setFont(e.getFont());
        g2.setStroke(new BasicStroke());
        
        FontMetrics fm = g2.getFontMetrics(e.getFont());
        if(this != null) {
            nameWidth = fm.stringWidth(e.getName()) + 2*PADDING ;
            if(nameWidth != getWidth()) {
                width = nameWidth;
                setWidth(nameWidth);
                setBounds(getLocation().x, getLocation().y);
            }
        }

        g2.setColor(Color.WHITE);
        g2.fill(contentShape);        
        Color color = Color.BLACK;
        if(e.isWarned() || e.isHasNameConflict()){
            color = Color.RED;
        }
        
        if(e.isHighlighted()) {
            g2.setColor(highlightColor);
            g2.setStroke(new BasicStroke(HIGHLIGHT_THICKNESS));
            g2.draw(highlightShape);
        }
        g2.setColor(color);
        if(e.isSelected())
            g2.setColor(SELECTED_COLOR);
        g2.draw(contentShape);
        
        g2.setColor(color);
        g2.drawString(e.getName(),
                (int)((RoundRectangle2D)contentShape).getX()+PADDING,
                (int)((RoundRectangle2D)contentShape).getY()+18-PADDING);
        
        if(isIsResizing()) {
            getUpperLeftHandle() .draw(g2);
            getUpperRightHandle().draw(g2);
            getLowerLeftHandle() .draw(g2);
            getLowerRightHandle().draw(g2);
        }
        // move associations
        
        if(e.getListAssociation().size() > 0){
            for(Association association : e.getListAssociation()) {
                Associable from = association.getFromAssociable();
                Associable to = association.getFromAssociable();
                
                if(from == null || to == null){
                     e.getListAssociation().remove(association);
                }else{
                    association.move(e);
                    association.draw(g2);
                }
            }
        }
    }
}
