package view.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.RoundRectangle2D;
import view.design.ColorPalette;
import view.element.Element;

public class PolygonShape extends ElementShape{
    public static final Double Width = 50.0;
    public static final Double Heigth = 50.0;
    
    
    public PolygonShape(){
        super();
        shapeType      = "agentShape";
        nameShape      = new Rectangle();
        contentShape   = new Polygon();
        for (int i = 0; i < 360; i++) {
                double t = i / 180.0;
                ((Polygon)contentShape).addPoint(
                        (int) (( (Width/2) * Math.cos(t * Math.PI))),
                        (int) (( (Heigth/2) * Math.sin(t * Math.PI))));
            }
        highlightShape = new Rectangle();
        
        lastX = 0;
        lastY = 0;

        width = 200;
        contentHeight = 100;
        nameWidth = 0;
    }

    
    public void draw(Graphics2D g2, Element e){
        if(this != null) {
            FontMetrics fm = g2.getFontMetrics(e.getFont());
            int nameWidth = fm.stringWidth(e.getName()) ;
            if(nameWidth > getWidth()) {
                setWidth(nameWidth + 2*PADDING);
                setBounds(getLocation().x, getLocation().y);
            }
        }

        g2.setStroke(new BasicStroke());
        g2.setColor(Color.LIGHT_GRAY);
        g2.fill(nameShape);
        g2.setColor(Color.BLACK);
        if(e.isSelected())
            g2.setColor(SELECTED_COLOR);
        g2.draw(nameShape);
        g2.drawPolygon((Polygon)contentShape);
        
        if(this != null) {
            g2.setColor(Color.BLACK);
            g2.setFont(e.getFont());
            g2.drawString(e.getName(),
                    (int)((Rectangle)nameShape).getX()+PADDING,
                    (int)((Rectangle)nameShape).getY()+NAME_HEIGHT-PADDING);
        }
        
        if(isIsResizing()) {
            getUpperLeftHandle().draw(g2);
            getUpperRightHandle().draw(g2);
            getLowerLeftHandle().draw(g2);
            getLowerRightHandle().draw(g2);
        }
        
        if(e.isWarned()){
            g2.setColor(ColorPalette.REDLIGHT);
            g2.setStroke(new BasicStroke(HIGHLIGHT_THICKNESS));
            g2.draw(highlightShape);
        }else if(e.isHighlighted()) {
            g2.setColor(highlightColor);
            g2.setStroke(new BasicStroke(HIGHLIGHT_THICKNESS));
            g2.draw(highlightShape);
        }
    }   
}
