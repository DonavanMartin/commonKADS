package view.shape;

import view.element.Association;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import view.element.Element;

public class MultiCircle extends ElementShape{
    
    public MultiCircle(){
        super();
        shapeType = "multiCircle";
        contentShape = new Ellipse2D.Double();
        highlightShape = new Ellipse2D.Double();
    }
    
    public void draw(Graphics2D g2, Element e) {
        //Operator operator = (Operator)e;
        
        if(e != null) {
            double nombreDor = (1+Math.sqrt(5.0))/2;
            if(e.isWarned()) {
                g2.setColor(Color.RED);
                g2.fill(contentShape);

                g2.setColor(Color.WHITE);
                g2.fill(getBlackCenterCircle(0.84));

                g2.setColor(Color.RED);
                g2.fill(getBlackCenterCircle(1/nombreDor));
            }
            else{
                g2.setColor(Color.BLACK);
                g2.fill(contentShape);

                g2.setColor(Color.WHITE);
                g2.fill(getBlackCenterCircle(0.84));

                g2.setColor(Color.BLACK);

                g2.fill(getBlackCenterCircle(1/nombreDor));
            }
            for(Association a : e.getFromAssociation()){
                a.draw(g2);
            }
            
            for(Association a : e.getPointerAssociation()){
                a.move(e);
                a.draw(g2);
            }

            // move associations
            if(e.getListAssociation().size() > 0){
                for(Association association : e.getListAssociation()) {
                    association.move(e);
                    association.draw(g2);
                }
            }
        }
    }
    
    private  Ellipse2D getBlackCenterCircle(double ratio){
        Ellipse2D center = new Ellipse2D.Double();
        double x = ((Ellipse2D.Double)contentShape).getX();
        double y = ((Ellipse2D.Double)contentShape).getY();
        double delta = ((double)RADIUS/2)*(1-ratio);
        center.setFrame(x+delta, y+delta, ratio*RADIUS, ratio*RADIUS);
        return center;
    }
}
