package view.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.RoundRectangle2D;
import view.element.Element;

public class TitleledRoundRectangle2D extends ElementShape{
    public static final Double arcWidth = 10.0;
    public static final Double arcHeigth = 10.0;
    public TitleledRoundRectangle2D(){
        super();
        shapeType = "titleledRoundRectangle";
        nameShape = new RoundRectangle2D.Float();
        contentShape = new RoundRectangle2D.Float();
        highlightShape = new RoundRectangle2D.Float();
        
        width = 200;
        contentHeight = 100;
        nameWidth = 0;
    }

    
    public void draw(Graphics2D g2, Element e){
        if(this != null) {
            FontMetrics fm = g2.getFontMetrics(e.getFont());
            int nameWidth = fm.stringWidth(e.getName()) ;
            if(nameWidth != getWidth()) {
                setWidth(nameWidth + 2*PADDING);
                setBounds(getLocation().x, getLocation().y);
            }
        }
        
        g2.setStroke(new BasicStroke());
        g2.setColor(Color.LIGHT_GRAY);
        g2.fill(nameShape);
        g2.setColor(Color.BLACK);
        if(e.isSelected())
            g2.setColor(SELECTED_COLOR);
        g2.draw(nameShape);
        g2.draw(contentShape);
        
        if(this != null) {
            g2.setColor(Color.BLACK);
            g2.setFont(e.getFont());
            g2.drawString(e.getName(),
                    (int)((RoundRectangle2D)nameShape).getX()+PADDING,
                    (int)((RoundRectangle2D)nameShape).getY()+NAME_HEIGHT-PADDING);
        }
        
        if(isIsResizing()) {
            getUpperLeftHandle() .draw(g2);
            getUpperRightHandle().draw(g2);
            getLowerLeftHandle() .draw(g2);
            getLowerRightHandle().draw(g2);
        }
        if(e.isWarned()) {
            g2.setColor(Color.RED);
            g2.setStroke(new BasicStroke(HIGHLIGHT_THICKNESS));
            g2.draw(highlightShape);
        }
        if(e.isHighlighted()) {
            g2.setColor(highlightColor);
            g2.setStroke(new BasicStroke(HIGHLIGHT_THICKNESS));
            g2.draw(highlightShape);
        }
    }
    
}
