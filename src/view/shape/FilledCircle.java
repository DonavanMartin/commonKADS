package view.shape;

import view.element.Association;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import view.element.Element;

public class FilledCircle extends ElementShape{
    
    public FilledCircle(){
        super();
        shapeType = "filledCircle";
        contentShape = new Ellipse2D.Double();
        highlightShape = new Ellipse2D.Double();
    }
    
     public void draw(Graphics2D g2, Element e) {
        //Operator operator = (Operator)e;
        
        if(e != null) {
            
            Color color = Color.BLACK;
            if(e.isWarned()) {
                color = Color.RED;
            }
            g2.setColor(Color.WHITE);
            g2.fill(contentShape);
            
            g2.setColor(color);
            if(e.isSelected())
                g2.setColor(SELECTED_COLOR);
            g2.setStroke(new BasicStroke());
            g2.draw(contentShape);
            g2.fill(contentShape);
            
                g2.setColor(color);
                g2.setFont(e.getFont());
            
            
         
            for(Association a : e.getFromAssociation()){
                a.draw(g2);
            }
            
            for(Association a : e.getPointerAssociation()){
                a.move(e);
                a.draw(g2);
            }
            
            if(e.getListAssociation().size() > 0){
                for(Association association : e.getListAssociation()) {
                    association.move(e);
                    association.draw(g2);
                }
            }
            
        }
    }
    
}
