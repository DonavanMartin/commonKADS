package view.shape;

import view.element.Association;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import view.element.Element;

public class LabeledCircle extends ElementShape{
    
    public LabeledCircle(){
        super();
        shapeType = "labeledCircle";
        contentShape = new Ellipse2D.Double();
        highlightShape = new Ellipse2D.Double();
    }
    
    @Override
    public void draw(Graphics2D g2, Element e) {
        if(e != null) {
            g2.setColor(Color.WHITE);
            g2.fill(contentShape);
            
            g2.setColor(Color.BLACK);
            if(e.isSelected())
                g2.setColor(SELECTED_COLOR);
            g2.setStroke(new BasicStroke());
            g2.draw(contentShape);
            
           if(e.isWarned()) {
                g2.setColor(Color.RED);
                g2.setStroke(new BasicStroke(HIGHLIGHT_THICKNESS));
                g2.draw(highlightShape);
            }else{
                g2.setColor(Color.BLACK);
                g2.setFont(e.getFont());
           }
            FontMetrics fm = g2.getFontMetrics(e.getFont());

            for(Association a : e.getFromAssociation()){
                a.draw(g2);
            }
            
            for(Association a : e.getPointerAssociation()){
                a.move(e);
                a.draw(g2);
            }
        }
    }
}
