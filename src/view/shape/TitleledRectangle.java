package view.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import model.element.commonKADS.Agent;
import view.element.Element;

public class TitleledRectangle extends ElementShape{
    public TitleledRectangle(){
        super();
        shapeType = "titleledRectangle";
        nameShape = new Rectangle();
        contentShape = new Rectangle();
        highlightShape = new Rectangle();
        
        width = 200;
        contentHeight = 300;
        nameWidth = 0;
    }
    
       public void draw(Graphics2D g2, Element e){
        if(this != null) {
            FontMetrics fm = g2.getFontMetrics(e.getFont());
            int nameWidth = fm.stringWidth(e.getName()) ;
            if(nameWidth > getWidth()) {
                setWidth(nameWidth + 2*PADDING);
                setBounds(getLocation().x, getLocation().y);
            }
        }
        
        g2.setStroke(new BasicStroke());
        g2.setColor(Color.LIGHT_GRAY);
        g2.fill(nameShape);
        g2.setColor(Color.BLACK);
        if(e.isSelected())
            g2.setColor(SELECTED_COLOR);
        g2.draw(nameShape);
        g2.draw(contentShape);
        
        if(this != null) {
            g2.setColor(Color.BLACK);
            g2.setFont(e.getFont());
            g2.drawString(e.getName(),
                    (int)((Rectangle)nameShape).getX()+PADDING,
                    (int)((Rectangle)nameShape).getY()+NAME_HEIGHT-PADDING);
        }
        
        if(isIsResizing()) {
            getUpperLeftHandle(). draw(g2);
            getUpperRightHandle().draw(g2);
            getLowerLeftHandle(). draw(g2);
            getLowerRightHandle().draw(g2);
        }
        
        if(e.isWarned() && !(e instanceof Agent)) {
            g2.setColor(Color.RED);
            g2.setStroke(new BasicStroke(HIGHLIGHT_THICKNESS));
            g2.draw(highlightShape);
        }
        
        if(e.isHighlighted()) {
            g2.setColor(highlightColor);
            g2.setStroke(new BasicStroke(HIGHLIGHT_THICKNESS));
            g2.draw(highlightShape);
        }
    }
    
}
