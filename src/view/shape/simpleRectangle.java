package view.shape;

import view.element.Associable;
import view.element.Association;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import view.element.Element;

public class simpleRectangle extends ElementShape{
    
    public simpleRectangle(){
        super();
        shapeType = "simpleRectangle";
        contentShape = new Rectangle();
        highlightShape = new Rectangle();
        
        width = 50;
        contentHeight = 20;
        nameWidth = 0;
        NAME_HEIGHT = 0;
    }
    
    public void draw(Graphics2D g2, Element e){
        
        g2.setFont(e.getFont());
        g2.setStroke(new BasicStroke());
        
        if(this != null) {
            FontMetrics fm = g2.getFontMetrics(e.getFont());
            nameWidth = fm.stringWidth(e.getName()) ;
            if(nameWidth != getWidth()) {
                width = nameWidth;
                setWidth(nameWidth + 2*PADDING);
                setBounds(getLocation().x, getLocation().y);
            }
        }
        
        g2.setColor(Color.WHITE);
        g2.fill(contentShape);
        
        if(e.isWarned()) {
            g2.setColor(Color.RED);
            g2.setStroke(new BasicStroke(HIGHLIGHT_THICKNESS));
            g2.draw(highlightShape);
        }
        
        g2.setColor(Color.BLACK);
        if(e.isSelected())
            g2.setColor(SELECTED_COLOR);
        g2.draw(contentShape);
        
        g2.setColor(Color.BLACK);
        g2.drawString(e.getName(),
                (int)((Rectangle)contentShape).getX()+PADDING,
                (int)((Rectangle)contentShape).getY()+18-PADDING);
        
        
        if(isIsResizing()) {
            getUpperLeftHandle() .draw(g2);
            getUpperRightHandle().draw(g2);
            getLowerLeftHandle() .draw(g2);
            getLowerRightHandle().draw(g2);
        }
        for(Association a : e.getFromAssociation()){
            a.draw(g2);
        }
        
        for(Association a : e.getPointerAssociation()){
            a.move(e);
            a.draw(g2);
        }

        if(e.getListAssociation().size() > 0){
            for(Association association : e.getListAssociation()) {
                Associable from = association.getFromAssociable();
                Associable to = association.getFromAssociable();
                
                if(from == null || to == null){
                    e.getListAssociation().remove(association);
                }else{
                    association.move(e);
                    association.draw(g2);
                }
            }
        }
    }
}
