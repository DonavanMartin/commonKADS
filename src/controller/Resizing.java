package controller;

import view.ui.Logs;
import java.awt.Point;
import java.util.ArrayList;
import view.element.Element;
import model.element.commonKADS.Agent;
import view.element.Corner;
import view.shape.ElementShape;
import view.element.Ownable;

public class Resizing {
    private Control control;
    private Element selectedElement;
    private boolean isResizing;
    private int lastIndex;
    private int elementIndex;
    
    public Resizing(Control controllers) {
        control = controllers;
        selectedElement = null;
        isResizing = false;
        control.log.printSuccess("initialization: ResizeController");
    }

    public boolean isResizing() {
        return isResizing;
    }
    
    public void beginResizing(Point point) {
        Element element = control.model.getElement(point);
        if(selectedElement != null)
            if(element != selectedElement)
                finishResizing();
        try{
            if(element != null ) {
                if(element instanceof Agent) {
                    selectedElement = element;
                    selectedElement.setIsSelected(true);
                    selectedElement.setIsResizing(true);
                    control.inclusionController.setSelectedOwnableForResizing((Ownable) selectedElement);
                    control.inclusionController.manageTempInclusions();
                    control.inclusionController.highlightOwner();
                    control.inclusionController.highlightContainedOwnables();
                    isResizing = true;
                }
            }
            control.CanvasController.updateCanvas();
        }catch(Exception e){
            control.log.printError("Failed to Begin Resizing: Verify class type and extends of:" + element.typeName);
        }
    }
    
    public void resize(Point point) {
        if(selectedElement != null) {
            if(selectedElement instanceof Agent){
                resizeAgent(point);
            }else{
                selectedElement.resize(point);
            }
   
            control.inclusionController.manageTempInclusions();
            control.inclusionController.highlightContainedOwnables();
            control.inclusionController.highlightOwner();
            control.CanvasController.updateCanvas();
           // fileController.saveFile();
        }
    }
    
    private void resizeAgent(Point p){
        
        int cornerHandle = ((Agent)selectedElement).getShape().getHandleType();
        

        if(cornerHandle == -1 ){
            control.log.printError("Failed to find type of corner ! from resizing point");
        }else{
            
            switch(cornerHandle){
                case ElementShape.LOWERLEFT:   resizeAgentsLowerLimit(p); resizePreviousWidth(p);  break;
                case ElementShape.UPPERLEFT:   resizePreviousWidth(p); break;
                case ElementShape.LOWERRIGHT: resizeCurrentAgentLowerRight(p); resizeAgentsLowerLimit(p);  resizeNextWidth(p); break;
                case ElementShape.UPPERRIGHT: resizeCurrentAgentUpperRight(p); resizeNextWidth(p);     break;
            }
        }   
        control.CanvasController.updateCanvas();
    }

    private boolean haveNextAgent(){
        ArrayList<Agent> agents  = control.model.getListAgent();
        lastIndex = agents.size() -1 ;
        elementIndex = agents.indexOf(selectedElement);

        return (lastIndex > elementIndex) ? true : false; 
    }

    private void resizeCurrentAgentUpperRight(Point p){
        if (haveNextAgent()){
            int minX = getCurrentMinX(p);
            int maxX  = getMaxX(p);
            int x = (p.x > minX) ? maxX : minX; 
            selectedElement.resize(new Point(x,0));
        }else{
            int x = getCurrentMinX(p);  
            selectedElement.resize(new Point(x,0));
        }
    }
    
    private void resizeCurrentAgentLowerRight(Point p){
        if (haveNextAgent()){
            int minX = getCurrentMinX(p);
            int maxX  = getMaxX(p);
            int x = (p.x > minX) ? maxX : minX; 
            selectedElement.resize(new Point(x,p.y));
        }else{
            int x = getCurrentMinX(p);
            selectedElement.resize(new Point(x,p.y));
        }
    }
    
    private void resizeCurrentAgentUpperLeft(Point p){
        if(0 == control.model.getListAgent().indexOf(selectedElement)){
            selectedElement.resize(new Point(0,0));
        }else{
            int x = getPreviousMinX(p);
            selectedElement.resize(new Point(x,0));
        }
    }
    
    private void resizeCurrentAgentLowerLeft(Point p){
        if(0 == control.model.getListAgent().indexOf(selectedElement)){
            selectedElement.resize(new Point(0,p.y));
        }else{
            int x = getPreviousMinX(p);
            selectedElement.resize(new Point(x,p.y));
        }
    }
    
    private int getPreviousMinX(Point p){
        ArrayList<Agent> agents  = control.model.getListAgent();
        elementIndex = agents.indexOf(selectedElement);
        Agent previousAgent = agents.get(elementIndex-1);
        int location = previousAgent.getPosition().x;
        int namewidth = previousAgent.getNameWidth();
        int minX = location+namewidth;
        return (minX<p.x)?p.x : minX;
    }
    
    private int getMaxX(Point p){
        ArrayList<Agent> agents  = control.model.getListAgent();
        elementIndex = agents.indexOf(selectedElement);
        Agent nextAgent = agents.get(elementIndex+1);
        int minX = nextAgent.getPosition().x;
        return (minX<p.x)?p.x : minX;
    }
    
    
    private int getCurrentMinX(Point p){
        int location = selectedElement.getPosition().x;
        int namewidth = selectedElement.getNameWidth();
        int minX = location+namewidth;
        return (minX<p.x)?p.x : minX;
    }
    
    
    /*private void resizeAgentsUpperLimit(Point p){
         ArrayList<Agent> agents  = control.model.getListAgent();
         for(Agent a : agents){
            a.setUpperLimit(p);
         }
    }*/
    
    
    private void resizeAgentsLowerLimit(Point p){
         ArrayList<Agent> agents  = control.model.getListAgent();
         for(Agent a : agents){
             a.setLowerLimit(p);
         }
    }
    private void resizeNextWidth(Point p){
        
        ArrayList<Agent> agents  = control.model.getListAgent();
        lastIndex = agents.size() -1 ;
        elementIndex = agents.indexOf(selectedElement);
        if(lastIndex > elementIndex){
            int x = getCurrentMinX(p); 
            Agent next = agents.get(elementIndex+1);
            int r_x = ((Agent)next).getShape().getUpperRightHandle().getLocation().x;
            int width = next.getNameWidth();
            x = (x < r_x-width)? x: r_x-width;
            agents.get(elementIndex+1).resizeLeft(x);
            agents.get(elementIndex).resizeRigth(x);
        }
    }
    private void resizePreviousWidth(Point p){
        ArrayList<Agent> agents  = control.model.getListAgent();
        elementIndex = agents.indexOf(selectedElement);
        if(elementIndex > 0){
            Agent prev = agents.get(elementIndex-1);
            int r_l = ((Agent)prev).getShape().getUpperLeftHandle().getLocation().x;
            int width = selectedElement.getNameWidth();
            int x = (p.x > r_l+width)? p.x: r_l+width;
            
            if(haveNextAgent()){
                Agent next = agents.get(elementIndex+1);
                int r_r = ((Agent)next).getShape().getUpperLeftHandle().getLocation().x;
                int max = r_r - selectedElement.getNameWidth();
                x = (x < max)? x : max;
            }
            
            agents.get(elementIndex-1).resizeRigth(x);
            agents.get(elementIndex).resizeLeft(x);
            
        }
    }
    
    public void finishResizing() {
        if(selectedElement != null) {
            control.inclusionController.resetHighlight();
            control.inclusionController.manageInclusions();
            selectedElement.setIsResizing(false);
            selectedElement.setIsSelected(false);
            selectedElement = null;
            isResizing = false;
            control.CanvasController.updateCanvas();
        }
    }
    
    public void setSelectedHandle(Point p) {
        if(selectedElement != null ){
            if (selectedElement instanceof Agent ){
                if(-1 != ((Agent)selectedElement).getShape().getHandleType(p))
                    selectedElement.setSelectedHandle(p);
            }else{
                selectedElement.setSelectedHandle(p);
            }
        }
    }
    
    public void unsetSelectedHandle() {
        if(selectedElement != null)
            selectedElement.unsetSelectedHandle();
    }
    
}
