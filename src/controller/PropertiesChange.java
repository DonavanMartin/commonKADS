package controller;

import view.ui.BottomPanel;
import view.ui.Logs;
import view.workSheets.WorkSheet;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class PropertiesChange implements Observer{
    private Control control;
    private BottomPanel bottomPanel;
    private ArrayList<WorkSheet> workSheets;
    
    public PropertiesChange(Control controllers){
        this.control = controllers;
        bottomPanel = new BottomPanel();
        bottomPanel.addLogsPanel(control.getLog());
        workSheets = null;
        control.log.printSuccess("initialization: PropertiesController");
    }
    public void setArrayListWorksheets(ArrayList<WorkSheet> workSheets){
        this.workSheets = workSheets;
    }
    
    public BottomPanel getBottomPanel(){
        return bottomPanel;
    };

    public void setWorksheetPanels(){
        bottomPanel.removeWorksheets();
        if(workSheets.size() > 0 ){
            try{
                for(WorkSheet w : workSheets){
                    bottomPanel.addWorkSheet(w);
                }
                //log.printInfo("Set worksheets completed");
            }catch(Exception e){
                control.log.printError("Element selected do not have worksheets.");
            }
        }else{
             control.log.printError("No worksheet set for this element. See element's class to add one.");
        }    
    }

    public void setBottomPanel(BottomPanel bottomPanel) {
        this.bottomPanel = bottomPanel;
    }

    public void setWorkSheets(ArrayList<WorkSheet> workSheets) {
        this.workSheets = workSheets;
    }
    
    public void update(){
        bottomPanel.getPropertiesTab().revalidate();
        bottomPanel.getPropertiesTab().updateUI();
    }
    public void addObserve(Observable o) {
        o.addObserver(this);
    }
    @Override
    public void update(Observable o, Object arg) {
        control.CanvasController.updateCanvas();
    }
 
}
