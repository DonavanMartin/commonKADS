package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import app.CommonKADSModel;
import app.commonKADSapp;

public class FileControl {
    private commonKADSapp userInterface;
    private Control control;
    
    private final JFileChooser fileSelector;
    private File file;
    private Document doc;
    private XPath path;
        
    
    public FileControl(Control control) {
        this.control = control;
       
        file = null;
        fileSelector = new JFileChooser();
        doc = null;
        control.log.printSuccess("initialization: FileController");

    }
    public boolean fileExists() {
        return file != null;
    }
    
    public void selectFileDialog() {
        fileSelector.setDialogTitle("Select a file");
        fileSelector.setDialogType(JFileChooser.CUSTOM_DIALOG);
        FileFilter filter = new ExtensionFilter();
        
        fileSelector.setFileFilter(filter);
        
        int choice = fileSelector.showDialog(userInterface, "Select");
        if(choice == JFileChooser.APPROVE_OPTION) {
            file = fileSelector.getSelectedFile();
            openFile();
            userInterface.setFileName(file.toString());
        }
        else if(file == null)
            closeFile();
    }
    
    private void openFile() {
        try {
            DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbfactory.newDocumentBuilder();
            XPathFactory xpfactory = XPathFactory.newInstance();
            path = xpfactory.newXPath();
            doc = builder.parse(file);
            readModel();
        } catch (FileNotFoundException e) {
            createFile();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(userInterface, "The format of the file \"" + file.toString() + "\" is not correct.", "Incorrect file format", JOptionPane.OK_OPTION);
            selectFileDialog();
        } catch (ParserConfigurationException | SAXException e) {
            JOptionPane.showMessageDialog(userInterface, "An error occured while opening the file \"" + file.toString() + "\".", "Open file error", JOptionPane.OK_OPTION);
        }
        control.CanvasController.updateCanvas();
    }
    
    private void readModel() {
        try {
            control.model = new CommonKADSModel(control);
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "An error occured while reading the file \"" + file.toString() + "\".", "Reading file error", JOptionPane.OK_OPTION);
        }
    }
    
    
    private void createFile() {
        
        file = new File(file.toString() + "." + ExtensionFilter.EXTENSION);
        newModel();
    }
    
    public void saveFile() {
        try {
            DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbfactory.newDocumentBuilder();
            doc = builder.newDocument();
            //doc.appendChild(writeModel());
            
            DOMImplementation impl = builder.getDOMImplementation();
            DOMImplementationLS implLS = (DOMImplementationLS) impl.getFeature("LS", "3.0");
            LSSerializer serializer = implLS.createLSSerializer();
            LSOutput output = implLS.createLSOutput();
            
            OutputStream outputStream = new FileOutputStream(file);
            output.setByteStream(outputStream);
            output.setEncoding("UTF-8");
            
            serializer.write(doc, output);
            
        } catch (ParserConfigurationException e) {
            JOptionPane.showMessageDialog(null, "An error occured while saving the file \"" + file.toString() + "\".", "Save file error", JOptionPane.OK_OPTION);
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "The file \"" + file.toString() + "\" cannot be found.", "File not found", JOptionPane.OK_OPTION);
        }
    }
    
    
    private void newModel() {
        try {
            file.createNewFile();
            control.model = new CommonKADSModel(control);
            control.CanvasController.updateCanvas();
           // saveFile();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(userInterface, "The file \"" + file.toString() + "\" could not be created.", "File", JOptionPane.OK_OPTION);
        }
    }
    
    public void closeFile() {
        int answer = JOptionPane.showConfirmDialog(null, "Do you want to close CommonKADS Modeler?", "Close application", JOptionPane.YES_NO_OPTION);
        if(answer == 0) {
            if(control.model != null)
               // saveFile();
            System.exit(0);
        }
        else if(control.model == null){
            selectFileDialog();
        }
    }
    
    class ExtensionFilter extends FileFilter {
        
        public static final String EXTENSION = "kads";
        
        public ExtensionFilter() {}
        
        @Override
        public boolean accept(File file) {
            boolean accept = false;
            
            if(file.isDirectory()) {
                accept = true;
            }
            else {
                String fileName = file.toString().toLowerCase();
                int index = fileName.lastIndexOf(".");
                if(index != -1) {
                    String fileExtension = fileName.substring(index + 1);
                    if(fileExtension.equalsIgnoreCase(EXTENSION))
                        accept = true;
                }
            }
            return accept;
        }
        
        @Override
        public String getDescription() {
            return "Ecological Constraints Model (." + EXTENSION + ")";
        }
    }
     public void setUserInterface(commonKADSapp ui) {
        userInterface = ui;
    }
}

