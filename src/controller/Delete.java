package controller;

import java.awt.Point;
import view.element.Associable;
import view.element.Element;


public class Delete {
    private Control control;
    
    public Delete(Control control) {
        this.control = control;
        control.log.printSuccess("initialization: DeleteController");
    }
    
    public void deleteElement(Point point) {
        Element element = control.model.getElement(point);
        if(element != null) {
            if(element instanceof Associable){
                ((Associable) element).removeAssociation();
            }    
            control.model.remove(element);
            //fileController.saveFile();
        }
        control.CanvasController.updateCanvas();
    }
}
