package controller;

import java.awt.Point;
import app.Helper.ElementStates;


public class CanvasControl {
    private Control control;
    private int canvasState;
    
    public CanvasControl(Control control) {
        this.control = control;
        canvasState = ElementStates.SELECT;
        control.log.printSuccess("initialization: CanvasController");
    }

    public void setCanvasState(int state) {
        canvasState = state;
    }
    
    public int getCanvasWidth() {
        return control.modelCanvas.getSize().width;
    }
    
    public int getCanvasHeight() {
        return control.modelCanvas.getSize().height;
    }
    
    public Point getLocationOnScreen() {
        return control.modelCanvas.getLocationOnScreen();
    }
    
    public int getCanvasState() {
        return canvasState;
    }
    
    public void updateCanvas() {
        control.modelCanvas.update();
        control.inclusionController.manageOwnables();
        control.associationController.manageAssociationWarning();
    }
    
    public void setDebugLocation(Point point ){
        control.modelCanvas.setDebugTitle(point);
    }
    
}
