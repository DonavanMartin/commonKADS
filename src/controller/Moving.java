package controller;

import app.Helper.ElementStates;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import view.element.Element;
import model.element.commonKADS.Agent;
import view.element.Ownable;

public class Moving {
    private Control control;
    private Element selectedElement;
    private int lastIndex;
    private int elementIndex;
    private boolean isAgent;
    private boolean agentsHaveSwitched;
    private Point initialElementLocation;
    
    public Moving(Control control) {
        this.control = control;
        selectedElement = null;
        control.log.printSuccess("initialization: MoveController");
    }
   
    public void beginMoving(Point point) {
        if(!control.inclusionController.isResizing()) {
            Element element = control.model.getElement(point);
            if(element != null) {
                selectedElement = element;
                selectedElement.setIsSelected(true);
                if(selectedElement.isOwnable() ==  ElementStates.OWNABLEELEMENT) {
                    control.inclusionController.setSelectedOwnableForMoving((Ownable) selectedElement);
                    mangaInclusion();
                }
                if(selectedElement instanceof Agent){
                    selectedElement.computeDraggingOffset(new Point(point.x,  selectedElement.getPosition().y));
                    isAgent =true;
                    agentsHaveSwitched = false;
                    initialElementLocation = selectedElement.getPosition();
                }else{
                    selectedElement.computeDraggingOffset(point);
                    control.CanvasController.updateCanvas();
                }
            }
        }
    }
    
    public void moveElement(Point point) {
        if(!control.inclusionController.isResizing()) {
            if(selectedElement != null) {
                control.model.setOnTop(selectedElement);
                if(selectedElement.isOwnable()  ==  ElementStates.OWNABLEELEMENT) {
                    mangaInclusion();
                }
                if(isAgent != true){
                    selectedElement.setLocationWithOffsets(point);
                }else{
                    movingAgent(point);
                }
                    
                control.CanvasController.updateCanvas();
               // fileController.saveFile();
            }
        }
    }
    
    private void movingAgent(Point p){
        moveNextAgent(p);
        movePrevious(p);
    }
    
    private void moveNextAgent(Point p){
        ArrayList<Agent> agents  = control.model.getListAgent();
        lastIndex = agents.size() -1 ;
        elementIndex = agents.indexOf(selectedElement);
        if(lastIndex > elementIndex){
            Agent nextAgent =  agents.get(elementIndex+1);
            int midx = nextAgent.getMidX();
            int rX = (int) ((Agent)selectedElement).getRightPointX().getX();
            
            if(midx < rX){
                nextAgent.computeSwitchOffset(nextAgent.getPosition());                
                nextAgent.setLocationWithOffsets(initialElementLocation);
                updatePosition(nextAgent, initialElementLocation);
                
                Point newPosition = nextAgent.getRightPointX();                
                initialElementLocation = newPosition;    
                
                selectedElement.computeSwitchOffset(selectedElement.getPosition());
                selectedElement.setLocationWithOffsets(newPosition);
                updatePosition((Agent)selectedElement, newPosition);

                switchAgent((Agent)selectedElement, nextAgent );
                finishMoving();
            }else{
                if(elementIndex != 0){
                    selectedElement.setLocationAgentWithOffsets( new Point(p.x,initialElementLocation.y));
                    updatePosition((Agent)selectedElement,  new Point(selectedElement.getPosition().x,initialElementLocation.y));
                }else if((p.x- selectedElement.getOffsetX())>0) {
                    selectedElement.setLocationAgentWithOffsets( new Point(p.x,initialElementLocation.y));
                    updatePosition((Agent)selectedElement,  new Point(selectedElement.getPosition().x,initialElementLocation.y));
                }
            }
        }
    }
    
    private void updatePosition(Agent a, Point p){
        a.updateMovingCorner(p);
        a.updateShape();
    }
    
    private void movePrevious(Point p){
        ArrayList<Agent> agents  = control.model.getListAgent();
        elementIndex = agents.indexOf(selectedElement);
        if(elementIndex > 0){
            Agent previousAgent =  agents.get(elementIndex-1);
            int midx = previousAgent.getMidX();
            int lL = (int) ((Agent)selectedElement).getPosition().getX();
            if(midx > lL){
                
                Point newPosition = previousAgent.getLeftPointX();
                selectedElement.computeSwitchOffset(selectedElement.getPosition());
                selectedElement.setLocationWithOffsets(newPosition);
                updatePosition((Agent)selectedElement, newPosition);
                
                Point newp = new Point(newPosition.x + selectedElement.getWidth(), newPosition.y);
                previousAgent.computeSwitchOffset(previousAgent.getPosition());
                previousAgent.setLocationWithOffsets(newp);
                updatePosition(previousAgent, newp);
                
                switchAgent((Agent)selectedElement, previousAgent );
                finishMoving();
                
            }else{
                selectedElement.setLocationAgentWithOffsets( new Point(p.x, initialElementLocation.y));
                updatePosition((Agent)selectedElement,  new Point(selectedElement.getPosition().x, initialElementLocation.y));
            }
        }
    }
    
    private void switchAgent(Agent a, Agent b){
        ArrayList<Agent> agents  = control.model.getListAgent();
        int indexA = agents.indexOf(a);
        int indexB = agents.indexOf(b);
        Collections.swap(agents, indexA, indexB);
        agentsHaveSwitched = true;
    }
    
    private void mangaInclusion(){
        control.inclusionController.manageTempInclusions();
        control.inclusionController.highlightOwner();
        control.inclusionController.highlightContainedOwnables();
    }

    public Element getSelectedElement() {
        return selectedElement;
    }
    
    public void finishMoving() {
        if(isAgent == true && selectedElement != null && agentsHaveSwitched == false){
            selectedElement.computeSwitchOffset(selectedElement.getPosition());
            selectedElement.setLocationWithOffsets(initialElementLocation);
            updatePosition((Agent)selectedElement, initialElementLocation);
        }else{
            agentsHaveSwitched = false;
        }
        
        isAgent = false;
        if(! control.inclusionController.isResizing()) {
             control.inclusionController.resetHighlight();
            if(selectedElement != null){
                if(selectedElement.isOwnable()  ==  ElementStates.OWNABLEELEMENT){
                     control.inclusionController.manageInclusions();
                }
                selectedElement.setIsSelected(false);
            }
            control.CanvasController.updateCanvas();
            selectedElement = null;
        }
    } 
}
