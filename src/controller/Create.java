package controller;

import app.Helper.ElementStates;
import java.awt.Point;

import view.element.Element;
import model.element.commonKADS.Agent;
import model.element.commonKADS.Decision;
import model.element.commonKADS.Final;
import model.element.commonKADS.Initial;
import model.element.commonKADS.KnowledgeAsset;
import model.element.commonKADS.Task;


public class Create{
    // Controller
    private Control control;
    
    public Create(Control control) {
        this.control = control;
        control.log.printSuccess("initialization: CreateController");
    }
    public void addTask(Point point){           addElement(new Task     (control.log),  point);}
    public void addInitialNode(Point point){    addElement(new Initial  (control.log),  point);}
    public void addDecisionNode(Point point){   addElement(new Decision (control.log),  point);}
    public void addFinalNode(Point point){      addElement(new Final    (control.log),  point);}
    public void addKnowledgeAsset(Point point){ addElement(new KnowledgeAsset(control.log), point);} 

    public void addAgent(Point point) {
        int size = control.getModel().getListAgent().size();
        control.log.printInfo(size + " Agent Found in model");
        
        Agent a = new Agent(control.log);
        
        if(size > 0){
            addElement(a, point);
            a.setImediateRight(control.getModel().getListAgent().get(size-1).getShape());
        }else{
            addElement(a, new Point(0,0));
            a.getShape().setFirstAgentHandleLocation();
        }
        control.CanvasController.updateCanvas();
    }

    private void addElement(Element e, Point point){
        control.propertiesController.addObserve(e);
        e.setLocationWithOffsets(point);
        control.model.add(e);
        createElement(e);
        control.log.printSuccess("Element: " +e.getTypeName() + " added to Point:("+ point.getX()+","+point.getY()+")");
        control.inclusionController.manageOwnables();
        control.associationController.manageAssociationWarning();
    }
    
    public void createElement(Element e) {
        
        if(e != null) {
             e.setSuperModel(control.model);
             
            switch (e.getType()) {
                //CommonKADS
                //case ElementStates.ACTIVITY       :basicProperties (e);       break;
                 case ElementStates.TASK            :createTask(e);             break;
                //case ElementStates.INPUT          :basicProperties(e);        break;
                //case ElementStates.OUTPUT         :basicProperties(e);        break;
                case ElementStates.INITIAL_NODE     :createInitialNode(e);      break;
                case ElementStates.FINAL_NODE       :createFinalNode(e);        break;
                case ElementStates.DECISION         :createDecisionNode(e);     break;
                //case ElementStates.CONTROL_FLOW   :basicProperties(e);        break;
                //case ElementStates.OBJECT_FLOW    :basicProperties(e);        break;
                case ElementStates.PROCESS          :createProcess(e);          break;
                case ElementStates.AGENT            :createAgent(e);            break;
                case ElementStates.KNOWLEDGEASSET   :createKnowledgeAsset(e);   break;
                
                default: System.err.println("ChangeProperties Not Set Yet for : "+ ElementStates.getName(e.getType()));
            }
        }
    }
    
    private void createDecisionNode(Element e){
        e.setId(control.model.getNextElementId());
        e.setName("New Decision Node", control.model.getCurrentElementIdNumber());
        update();
    }
    private void createInitialNode(Element e){
        e.setId(control.model.getNextElementId());
        e.setName("New Inital Node", control.model.getCurrentElementIdNumber());
        update();
    }
    private void createFinalNode(Element e){
        e.setId(control.model.getNextElementId());
        e.setName("New Final Node", control.model.getCurrentElementIdNumber());
        update();
    }
    
    private void createKnowledgeAsset(Element e){
        e.setId(control.model.getNextElementId());
        e.setName("New KnowledgeAsset", control.model.getCurrentElementIdNumber());
        update();
    }
    
    private void createTask(Element e){
        e.setId(control.model.getNextElementId());
        e.setName("New Task", control.model.getCurrentElementIdNumber());
        update();
    }
    
    private void createAgent(Element e){
        e.setId(control.model.getNextElementId());
        e.setName("New Agent", control.model.getCurrentElementIdNumber());
        update();
    }
    
    private void createProcess(Element e){
        e.setId(control.model.getNextElementId());
        e.setName("New Process", control.model.getCurrentElementIdNumber());
        update();
    }
    
    private synchronized void update(){
        control.CanvasController.updateCanvas();
    }   
    
    
    
    
    
    
    // TODO Section
    public void addActivity(Point point) {
        control.log.printError("CreateController.java :addActivity() not implemented");
        /*
        addElement(Element e, Point point)*/
    }
    public void addInput(Point point) {
        control.log.printError("CreateController.java :addInput() not implemented");
        /*
        addElement(Element e, Point point){*/
    }
    public void addOutput(Point point) {
        control.log.printError("CreateController.java : addOutput() not implemented");
        /*
        addElement(Element e, Point point){*/
    }
    public void addKnowledgeFlow(Point point) {
        control.log.printError("CreateController.java : addControlFlow() not implemented");
        /*
        addElement(Element e, Point point){*/
    }
    public void addTaskFlow(Point point) {
        control.log.printError("CreateController.java : addObjectFlow() not implemented");
        /*
        addElement(Element e, Point point){*/
    }
}

