package controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import app.CommonKADSModel;
import app.Helper.ElementStates;
import view.workSheets.WorkSheet;
import view.table.jTableTools;
import java.awt.Point;
import java.awt.geom.Line2D;
import javax.swing.JTable;
import model.element.commonKADS.DecisionFlow;
import view.element.Association;
import view.element.Element;

public class MouseListeners implements MouseListener, MouseMotionListener {
    private Control control;
    private static final int HIT_BOX_SIZE = 20;

    
    public MouseListeners(Control controllers) {
        this.control = controllers;
        control.log.printSuccess("initialization: MouseListeners");
        
    }
    
    /* Onclick */
    @Override
    public void mouseClicked(MouseEvent e) {
        int canvasState = control.CanvasController.getCanvasState();
        Element element = control.model.getElement(e.getPoint());
        
        if(e.getButton() == MouseEvent.BUTTON1) {
            switch (canvasState) {
                // Basic operation
                case ElementStates.SELECT           :control.resizeController.beginResizing(e.getPoint()); break;
                case ElementStates.DELETE           :control.associationController.removeAssociation(e.getPoint());
                                                     control.deleteController.deleteElement(e.getPoint()); break;
                //CommonKADS
                case ElementStates.ACTIVITY         :control.create.addActivity     (e.getPoint());  break;
                case ElementStates.TASK             :control.create.addTask         (e.getPoint());  break;
                case ElementStates.INPUT            :control.create.addInput        (e.getPoint());  break;
                case ElementStates.OUTPUT           :control.create.addOutput       (e.getPoint());  break;
                case ElementStates.INITIAL_NODE     :control.create.addInitialNode  (e.getPoint());  break;
                case ElementStates.DECISION         :control.create.addDecisionNode(e.getPoint()) ;  break;
                case ElementStates.FINAL_NODE       :control.create.addFinalNode    (e.getPoint());  break;
                case ElementStates.KNOWLEDGE_FLOW   :control.create.addKnowledgeFlow(e.getPoint());  break;
                case ElementStates.TASK_FLOW        :control.create.addTaskFlow     (e.getPoint());  break;
                case ElementStates.AGENT            :control.create.addAgent        (e.getPoint());  break;
                case ElementStates.KNOWLEDGEASSET   :control.create.addKnowledgeAsset(e.getPoint()); break;
                
                default: break;
            }
        }
        
        // Right-Click
        else if(e.getButton() == MouseEvent.BUTTON3) {
            if(canvasState == ElementStates.SELECT) {
                control.resizeController.finishResizing();
                //element = model.getElement(e.getPoint());
            }
        }
    }
    
    /* Drag and Drop Event handler */
    @Override
    public void mousePressed(MouseEvent e) {
        int canvasState = control.CanvasController.getCanvasState();
        if(e.getButton() == MouseEvent.BUTTON1)
            if(canvasState == ElementStates.SELECT) {
                
                control.resizeController.setSelectedHandle(e.getPoint());
                control.moveController.beginMoving(e.getPoint());
                
                // Properties Tab
                displayWorksheet(e.getPoint());
            }
            else if(canvasState == ElementStates.FLOW ) {
                control.associationController.beginAssociation(e.getPoint());
            }
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        int canvasState = control.CanvasController.getCanvasState();
        
        if(e.getButton() == MouseEvent.BUTTON1)
            if(canvasState == ElementStates.SELECT) {
                control.resizeController.unsetSelectedHandle();
                control.moveController.finishMoving();
                
                Element element = control.model.getElement(e.getPoint());
                if(element != null){
                    updateCellDimension(element);
                }
            }
            else if(canvasState == ElementStates.FLOW ) {
                control.associationController.finishAssociation(e.getPoint());
            }
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
        int canvasState = control.CanvasController.getCanvasState();
        if(canvasState == ElementStates.SELECT) {
            if(control.resizeController.isResizing()){
                control.resizeController.resize(e.getPoint());
            }else{
                control.moveController.moveElement(e.getPoint());
            }
        }
        else if(canvasState == ElementStates.FLOW ) {
            control.associationController.moveAssociation(e.getPoint());
        }
        updateDebugLocation(e);
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        updateDebugLocation(e);
    }
    
    private void updateCellDimension(Element element){
        for(WorkSheet w : element.getWorksheets()){
            jTableTools.updateRowHeights((JTable) w.getTable());
            jTableTools.setTableheight(  (JTable) w.getTable());
        };
    }
    
    private void updateDebugLocation(MouseEvent e){
        control.CanvasController.setDebugLocation(e.getPoint());
        control.CanvasController.updateCanvas();
    }
    
    private void displayWorksheet(Point p){
        // Properties Tab
        Element element = control.model.getElement(p);
        if(element != null){
            control.propertiesController.setWorkSheets(element.getWorksheets());
            control.propertiesController.setWorksheetPanels();
            control.propertiesController.update();
            element.getProperties().tableUpdateNameElement();
        }else{
            displayAssociationWorkSheet(p);
        }
    }
    private void displayAssociationWorkSheet(Point p){
        Association a = getClickedLine(p.x, p.y);
        if(a != null){
            if(a instanceof DecisionFlow){
                control.propertiesController.setWorkSheets(a.getWorksheets());
                control.propertiesController.setWorksheetPanels();
                control.propertiesController.update();
                a.getProperties().tableUpdateNameElement();
            }
        }   
    }
    
    public Association getClickedLine(int x, int y) {
        int boxX = x - HIT_BOX_SIZE / 2;
        int boxY = y - HIT_BOX_SIZE / 2;
        
        int width  = HIT_BOX_SIZE;
        int height = HIT_BOX_SIZE;
        
        for (Association a  : control.model.getListAssociation()) {
            Line2D line = a.getLine();
            if (line.intersects(boxX, boxY, width, height)) {
                return a;
            }
        }
        return null;
    }
}
