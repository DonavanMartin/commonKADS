package controller;

import java.util.ArrayList;
import model.element.commonKADS.Agent;

import view.element.Element;
import view.table.TableEachRowRendererEditor;
import view.table.jTableTools;
import view.element.Ownable;


public class Inclusion{
    private Control control;
    
    private Element tempOwner;
    private Element selectedOwnable;
    private ArrayList<Ownable> listTempOwnable;
    
    private boolean isResizing;
    
    public Inclusion(Control control) {
        this.control = control;
        
        tempOwner = null;
        selectedOwnable = null;
        listTempOwnable = new ArrayList<Ownable>();
        isResizing = false;
        control.log.printSuccess("initialization: InclusionController");
    }
    
    public void manageTempInclusions() {
        listTempOwnable.clear();
        resetHighlight();
        
        // assign new visual owner
        tempOwner = null;
        for(int i = 0; i < control.model.getListVisualElementSize(); i++) {
            Element e = control.model.getVisualElement(i);
            if((e.contains((Element)selectedOwnable)) && (e.isSmallerThan(tempOwner)))
                tempOwner = e;
        }
        
        // assign new visual ownables
        if(selectedOwnable instanceof Element) {
            Element selectedConcept = (Element) selectedOwnable;
            for(int i = 0; i < control.model.getListOwnableElementSize(); i++) {
                Element ownableElement = (Element) control.model.getOwnableElement(i);
                if ((selectedConcept.contains((Element)ownableElement)) && (!containsOwner(ownableElement)))
                    listTempOwnable.add((Ownable) ownableElement);
                else
                    listTempOwnable.remove(ownableElement);
            }
        }
    }
    
    
    private void manageTempInclusions(Element agent) {
        listTempOwnable.clear();
        resetHighlight();
        
        // assign new visual owner
        tempOwner = null;
        for(int i = 0; i < control.model.getListVisualElementSize(); i++) {
            Element e = control.model.getVisualElement(i);
            if((e.contains((Element)agent)) && (e.isSmallerThan(tempOwner)))
                tempOwner = e;
        }
        
        // assign new visual ownables
        if(agent instanceof Element) {
            for(int i = 0; i < control.model.getListOwnableElementSize(); i++) {
                Element ownableElement = (Element) control.model.getOwnableElement(i);
                if ((agent.contains((Element)ownableElement)) && (!containsOwner(ownableElement))){
                    listTempOwnable.add((Ownable) ownableElement);
                }else{
                    listTempOwnable.remove(ownableElement);
                }
            }
        }
    }
    
    private boolean containsOwner(Element ownableElement) {
        boolean contains = false;
        
        Element owner = ownableElement.getOwner();
        if(owner != null)
            contains = selectedOwnable.contains(owner);
        
        return contains;
    }
    
    public void manageInclusions() {
        manageOwner();
        if(selectedOwnable instanceof Element)
            manageOwnables();
        tempOwner = null;
        selectedOwnable = null;
        listTempOwnable.clear();
        isResizing = false;
    }
    
    private void manageOwner() {
        Element e =  selectedOwnable.getOwner();
        if(e != tempOwner) {
            if(e != null){
                e.removeOwnableElement((Ownable) selectedOwnable);
                selectedOwnable.setOwner(tempOwner);
                if(tempOwner != null){
                    tempOwner.addOwnableElement((Ownable) selectedOwnable);
                }
            }
        }
    }
    
    public void manageOwnables() {
        if(!agentMoving()){
            clearOwnable();
            manageOwnablesAgent();
            manageNullOwnable();
        }
        //tempOwner = null;
        listTempOwnable.clear();
        //isResizing = false;
    }
    private boolean agentMoving(){
        return(control.moveController.getSelectedElement() != null)? true:false;
    }
    
    private void clearOwnable(){
        for(int i = 0; i < control.model.getListElement().size(); i++) {
            Element ownable = (Element) control.model.getListElement().get(i);
            ownable.setOwner(null);
        }
    }
    
    private void clearOwnableFromOwner(Element o){
        for(int i = 0; i < control.model.getListElement().size(); i++) {
            Element ownable = (Element) control.model.getListElement().get(i);
            ownable.removeOwnableElement((Ownable)o);
            ownable.setOwner(null);
        }
    }
    
    private void manageOwnablesAgent(){
        for(Element e : control.model.getListAgent()){
            selectedOwnable = e;
            manageTempInclusions(e);
            for(int i = 0; i < control.model.getListOwnableElementSize(); i++) {
                Element ownable = (Element) control.model.getOwnableElement(i);
                if(listTempOwnable.contains(ownable)) {
                    Element visualOwner = (Element) ownable.getOwner();
                    if(visualOwner != null){
                        visualOwner.removeOwnableElement((Ownable) ownable);
                         clearOwnableFromOwner(ownable);
                    }
                   
                    ownable.setOwner(e);
                    e.addOwnableElement((Ownable) ownable);
                }
                else {
                    Element currentVisualOwner = (Element) ownable.getOwner();
                    if(currentVisualOwner == e) {
                        currentVisualOwner.removeOwnableElement((Ownable) ownable);
                        ownable.setOwner(null);
                    }
                }
            }
        }
    }
    
    public void manageNullOwnable(){
        TableEachRowRendererEditor table = control.propertiesController.getBottomPanel().getWarningTable();
        removeNULLowner(table);
        addNULLowner(table);
    }
    
    private synchronized void removeNULLowner(TableEachRowRendererEditor t){
        jTableTools.removeAllProperties(t,"Null Owner");
    }
    
    private synchronized void addNULLowner(TableEachRowRendererEditor t){
        ArrayList<Element> es = control.model.getListElement();
        for(Element e : es){
            if(!(e instanceof Agent) && e instanceof Ownable){
                if(e.getOwner() == null){
                    jTableTools.add_property_row(t, "Null Owner", e.getClass().getName());
                }
            }
        }
        jTableTools.update(t);
    }
    
    
    public void highlightContainedOwnables() {
        if(selectedOwnable instanceof Element)
            for(Ownable ownable : listTempOwnable)
                ownable.activateContainedHighlight();
    }
    
    public void highlightOwner() {
        if(tempOwner != null)
            tempOwner.activateOwnerHighlight();
    }
    
    public void resetHighlight() {
        for(int i = 0; i < control.model.getListOwnableElementSize(); i++)
            control.model.getOwnableElement(i).resetHighlight();
    }
    
    public boolean isResizing() {
        return isResizing;
    }
    
    public void setSelectedOwnableForResizing(Ownable ownableElement) {
        selectedOwnable = (Element) ownableElement;
        isResizing = true;
    }
    
    public void setSelectedOwnableForMoving(Ownable ownableElement) {
        selectedOwnable = (Element) ownableElement;
    }
    
}