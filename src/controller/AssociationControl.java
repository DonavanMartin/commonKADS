package controller;

import view.element.Association;
import view.element.Associable;
import java.awt.Point;
import java.util.ArrayList;
import model.element.commonKADS.Decision;
import model.element.commonKADS.DecisionFlow;
import model.element.commonKADS.Final;
import model.element.commonKADS.Initial;
import view.element.Element;
import model.element.commonKADS.KnowledgeAsset;
import model.element.commonKADS.KnowledgeFlow;
import model.element.commonKADS.Task;
import model.element.commonKADS.TaskFlow;
import view.table.TableEachRowRendererEditor;
import view.table.jTableTools;

public class AssociationControl {
    private Control control;
    
    private Association  association;
    private Associable from;
    private Associable to;
    
    private boolean dashed;
    
    public AssociationControl(Control control) {
        this.control = control;
        association = null;
        from = null;
        to = null;
        control.log.printSuccess("initialization: AssociationController");
    }
    
    public void beginAssociation(Point point) {
        Element e =  control.model.getElement(point);
        if(e != null) {
            if(e instanceof Associable) {
                if(!(e instanceof Final)){
                    dashed = (e instanceof KnowledgeAsset)? true : false;
                    if(dashed){
                        association = new KnowledgeFlow(control.log);
                    }else if(e instanceof Decision){
                        association = new DecisionFlow(control.log);
                    }else
                        association = new TaskFlow(control.log);
                    from = (Associable) e;
                   
                    association.setFrom(from);
                    from.addAssociation(association);
                }
            }
        }
    }
    public void finishAssociation(Point point) {
        if(association != null) {
            Element element = control.model.getElement(point);
            if(element != null) {
                if(element != association.getFromAssociable()){
                    manageAssociation(element);
                }
            }else{
                reset();
            }
            control.CanvasController.updateCanvas();
            return;
        }
    }
    
    private void manageAssociation(Element e){
        if(e instanceof Associable) {
            if(!(e instanceof Initial)){
                to = (Associable) e;
                if(from != to){
                    if( from instanceof Decision){
                        manageDecisionAssociation(e);
                        return;
                    }else{
                        manageNormalAssociation(e);
                        return;
                    }
                    
                }
            }
        }
    }
    private void manageDecisionAssociation(Element e){

        if((from instanceof Decision && to instanceof Task)){
            addDecisionFlow(e);
        }
    }
    private void addDecisionFlow(Element e){
        from.removeAssociation(association);

        association = new DecisionFlow(control.log);
        association.setFrom(from);
        association.setTo(to);
        
        to.  addAssociation(association);
        from.addAssociation(association);
        
        from.addOutput(to);
        to.  addInput(from);
        control.model.addAssociation(association);
        control.CanvasController.updateCanvas();
        
    }
    private void manageNormalAssociation(Element e){
       from.removeAssociation(association);
        
       association =  (dashed || e instanceof KnowledgeAsset)
                ? new KnowledgeFlow(control.log) : 
                  new TaskFlow(control.log);
       
        association.setFrom(from);
        association.setTo(to);
        
        to.  addAssociation(association);
        from.addAssociation(association);
        
        from.addOutput(to);
        to.  addInput(from);
        control.model.addAssociation(association);
        control.CanvasController.updateCanvas();
    }
    
    private void reset(){
        if(from != null){
            from.removeAssociation(association);
        }
        if(to != null){
            to.removeAssociation(association);
        }
        association = null;
        from = null;
        to = null;
    }
    
    public void moveAssociation(Point point) {
        if(association != null) {
            association.setToPoint(point);
            control.CanvasController.updateCanvas();
        }
    }
    
    public void removeAssociation(Point point) {
        Element e = control.model.getElement(point);
        if(e != null && e instanceof Associable){
            ((Associable)e).removeAssociation();
        }
        control.model.removeAssociation(association);
        control.CanvasController.updateCanvas();
    }
    
     public void manageAssociationWarning(){
        TableEachRowRendererEditor table = control.propertiesController.getBottomPanel().getWarningTable();
        removeAssociationWarning(table);
        addAssociationWarning(table);
    }
    
    private synchronized void removeAssociationWarning(TableEachRowRendererEditor t){
        jTableTools.removeAllProperties(t,"Decision flow");
    }
    
    private synchronized void addAssociationWarning(TableEachRowRendererEditor t){
        ArrayList<Element> es = control.model.getListElement();
        for(Element e: es){
            if(e instanceof Decision ){
                if(((Decision)e).isInOutWarning()){
                    jTableTools.add_property_row(t, "Decision flow", e.getClass().getName());
                }
            }
        }
        jTableTools.update(t);
    }   
}