package controller;

import view.ui.Logs;
import app.CommonKADSModel;
import view.ui.CanvasCommonKADS;
import view.ui.ToolBar;

public class Control {
    protected AssociationControl associationController;
    protected CanvasControl CanvasController;
    protected Create create;
    protected Delete deleteController;
    protected Moving moveController;
    protected PropertiesChange propertiesController;
    protected Resizing resizeController;
    protected Inclusion inclusionController;
    protected ToolBar toolBar;
    private   FileControl fileController;
    protected Logs log;
    private final MouseListeners listener;
    protected CanvasCommonKADS modelCanvas;

    protected CommonKADSModel model;
    
    public Control(FileControl fileController , Logs log){
        this.modelCanvas = null;
        
        
        this.log =log;
        this.fileController = fileController; 
        model                    = new CommonKADSModel(this);
        associationController    = new AssociationControl(this);
        CanvasController         = new CanvasControl     (this);
        create                   = new Create     (this);
        deleteController         = new Delete     (this);
        moveController           = new Moving       (this);
        propertiesController     = new PropertiesChange (this);
        resizeController         = new Resizing     (this);
        inclusionController      = new Inclusion  (this);
        
        listener = new MouseListeners(this);
        log.printSuccess("initialization: Controllers");
    }

    public void setCanvas(CanvasCommonKADS modelCanvas){
        this.modelCanvas = modelCanvas;
    }
    
    public MouseListeners getListener() {
        return listener;
    }

    public CommonKADSModel getModel() {
        return model;
    }

    public CanvasControl getCanvasController() {
        return CanvasController;
    }

    public PropertiesChange getPropertiesController() {
        return propertiesController;
    }

    public AssociationControl getAssociationController() {
        return associationController;
    }

    public Create getCreate() {
        return create;
    }

    public Delete getDeleteController() {
        return deleteController;
    }

    public Moving getMoveController() {
        return moveController;
    }

    public Resizing getResizeController() {
        return resizeController;
    }

    public Inclusion getInclusionController() {
        return inclusionController;
    }

    public ToolBar getToolBar() {
        return toolBar;
    }

    public FileControl getFileController() {
        return fileController;
    }

    public Logs getLog() {
        return log;
    }
    
}
